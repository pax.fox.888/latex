\clearpage % flushes out all floats
\newpage
\section{Der Ursprung der Industriellen Revolution in England und die darauf folgende Ausbreitung in Europa}

\glqq Revolution\grqq{} wird meistens mit Gewalt verbunden, nicht so die Industrielle Revolution. Revolutionär war der Vorgang dennoch:
    \begin{itemize}
        \item Maschinenarbeit anstelle von Handarbeit.
        \item Entstehung des Fabrikwesens.
        \item Erschliessung neuer Energiequellen.
        \item Enorme Steigerung der Produktivität.
        \item Intensivierte Kommunikation.
        \item Die menschlichen Lebensformen veränderten sich als Folge des wirtschaftlichen Wandels.
    \end{itemize}

Der Begriff \glqq Industrielle Revolution\grqq{} wurde das erste Mal 1837 vom französischen Nationalökonomen Louis Auguste Blanqui verwendet. Neben der Industriellen Revolution wird die Industrialisierung 
    unterschieden. Damit wird die tiefgreifende wirtschaftliche Veränderung bezeichnet, welche durch ständige Weiterentwicklung gekennzeichnet ist. Sie setzte in England 1770 ein und hält bis heute an. \par

Der Ursprung der Industrialisierung ist vielfältig:
    \begin{itemize}
        \item Da in Europa die vielen Machtinhaber jeweils ihre wirtschaftliche und politische Position gegenüber den Anderen verbessern wollten, förderten sie die wirtschaftliche Entwicklung und somit das Wachstum  
                  ihres Reiches. Dies erreichten sie durch \textbf{Handel} von Güter/Produkten wie Holz, Getreide, Wein, Wolle, Meeresfrüchte etc.
        \item Der Handel dehnte sich mit der Zeit auf immer mehr Länder aus. Wohlstand und reiche Zentren entstanden, z.B. die Hansestädte oder die italienischen Stadtstaaten.
        \item Durch den Ausbau des Handels innerhalb Europas und nach Übersee bildete sich ein internationales \textbf{Kredit- und Bankensystem}.
        \item Die \glqq\textbf{Autonomie des Denkens}\grqq{}, wo Innovation und Neugierde als positiv galten, wurde gefördert. Die Allmacht der Kirche verfiel. Das Experiment als Forschungsmethode wurde entwickelt.
        \item Die Verbreitung von neuem Wissen geschah schnell: Dies dank der \textbf{lateinischen Sprache} und des entwickelten \textbf{Post- und Kuriersystems}.
    \end{itemize}

All dies intensivierte sich im 18. Jahrhundert, und dank günstigen politischen, wirtschaftlichen und sozialen Rahmenbedingungen begann die Industrialisierung in England.

\subsection{Gründe für die Vorreiterrolle Englands}
\subsubsection{Gesellschaftsstruktur}

Die \textbf{offene Gesellschaftsstruktur} erlaubte flexible Reaktionen auf wirtschaftliche Herausforderungen. Die gesellschaftlichen Schichten waren durchlässig, weil nur der älteste Sohn den Titel und den Besitz 
    des Vaters erbte. Man war investitionsfreudig und hatte einen ganz anderen Bezug zu Geld als z.B. Frankreich, wo der Adel bei Investitionen um den Verlust des über Generationen angehäuften Reichtums fürchtete.

In England setzte sich die \textbf{liberale Wirtschaftsordnung} durch. Ziele davon waren, ohne Handelsschranken oder Zunftvorschriften gewinnbringend zu investieren. Adam Smith hatte diese Theorie bereits im 18. 
    Jahrhundert verfasst.

\subsubsection{Bedeutung der Kolonien}

Dank der Kolonien und der Handels- und Kriegsflotte (bestehend aus Schiffen) entwickelte sich der \textbf{Aussenhandel} stark. Schutzgesetze sicherten englischen Schiffen und Bürgern Sonderrechte. Sie importierten 
    Gold, Silber, Tabak, Zucker, Kaffee, Tee, Baumwolle und Seide. Sie machten sich auch des Sklavenhandels gebrauch. Sie erschlossen die \textbf{Kolonien als Märkte} für englische Waren, wobei z.B. Wolltuch, Eisenwaren und andere Fertigwaren exportiert wurden. \par

Überseehandel und Seekriege forderten viele Schiffe mit guten Ausrüstungen. Es entstanden immer mehr Werften und Lagerhäuser in den Hafenstädten. \textbf{}London stieg zur Hauptstadt des Welthandels auf, eine neue 
    bürgerliche Gesellschaftsschicht, welche sehr vermögend war, konzentrierte sich dort. Je mehr Staaten dem britischen Modell folgten, desto heftiger wurden die internationale Verteilkämpfe um Rohstoff- und 
    Exportmärkte. Insofern hingen Imperialismus und Industrialisierung eng zusammen.

\subsubsection{Agrarreform, Bevölkerungswachstum und Verstädterung}

Kleine Bauern gaben ihr Land an grössere Besitzer ab, weil sie nicht mehr rentierten. Eine besondere Rolle spielten die \textbf{enclosures}. Unbebautes Land und die Allmend wurden privatisiert und unter den Pflug 
    genommen. Durch den Verlust der Allmend verloren viele kleine Besitzer ihre Möglichkeit ihrer (die der Allmend) Holznutzung und Viehweide-Nutzung. Einige Bauern konnten als Pächter weiterarbeiten, die Meisten wurden jedoch zu \textbf{Landarbeitern}. Die nun grossen Betriebe konnten im Getreidebau und der Viehzucht mehr produzieren. \par

Gleichzeitig nahm die \textbf{Bevölkerung} zu, viele wanderten in die Städte ab, weil sie auf dem Land keine Arbeit fanden. London hatte um 1800 knapp eine Million Einwohner. In dieser Zeit entstanden viele neue 
    Zentren aus Dörfern. Durch die wachsende Bevölkerung wuchs die Nachfrage und so der Inländische Markt. Das Verkehrswesen wurde ausgebaut.

\subsubsection{Höhere Produktivität dank Arbeitsteilung}

Adam Smith sagte 1778, dass die Steigerung der Arbeit, die die gleiche Zahl von Menschen durch die \textbf{Arbeitsteilung} erlangten, von drei Faktoren abhängt:
    \begin{enumerate}
        \item Die grössere Geschicklichkeit jedes einzelnen Arbeiters.
        \item Die Zeitersparnis, welche beim Wechseln von einer Tätigkeit zur Anderen verloren geht.
        \item Die Erfindung von Maschinen, welche die Arbeit erleichtern, die Arbeitszeit verkürzen und somit den Einzelnen in den Stand setzen, die Arbeit vieler zu leisten.
    \end{enumerate}

\subsection{Der Durchbruch der Industrialisierung in England}

Die Textilindustrie fungierte als Vorreiterin. Die erste Phase der Industrialisierung ist durch die Baumwolle geprägt. Mit ihrer Verarbeitung wurde der Schritt zur Fabrikarbeit vollzogen. Sie spielte eine grosse 
    Rolle:
    \begin{itemize}
        \item Steigender Bedarf nach Textilien konnte leichter und billiger als mit Wolle befriedigt werden.
        \item Baumwolle wurde in Indien und den südlichen Kolonien von Nordamerika angebaut.
        \item Die fertigen Tücher konnten in den Kolonien, aber auch im Inland und in Europa abgesetzt werden.
    \end{itemize}

Schlüsselerfindungen während dieser Zeit waren die folgenden:
    \begin{itemize}
        \item 1764: James Hargreaves konstruierte eine Maschine mit acht Spindeln (\glqq spinning jenny\grqq{}).
        \item 1769: Richard Arkwright stellte eine Maschine mit Wasserradantrieb vor.
        \item 1769: James Watt erfindet die Dampfmaschine, welche die mechanischen Spinnereien als auch Maschinelle Pumpen für den Bergbau antrieb.
        \item 1778: Samuel Crompton verband beides zu einem Modell, dass 20 bis 50 Spindeln hatte (\glqq Mule\grqq{}).
        \item 1784: Edmund Cartwright erfindet den mechanischen Webstuhl.
    \end{itemize}

Der Eisenbahnbau treibt die Industrielle Revolution voran. 1814 baute George Stephenson die erste leistungsfähige Lokomotive für Grubentransporte. 1825 wurde in der Grafschaft Durham die erste 27km lange 
    Eisenbahnstrecke über Tage eröffnet. Dieses neue Verkehrsmittel konnte Menschen schneller verbinden und senkte die Transportkosten für Massengüter deutlich. Die Eisenbahn heizte die Nachfrage nach hochwertigem Eisen somit an.

\subsection{Periodisierung}
\subsubsection{Protoindustrialisierung}

Die Industrialisierung begann nicht plötzlich sondern war ein gradueller Prozess. Die erste Phase wird Frühindustrialisierung/Protoindustrialisierung genannt und ging der eigentlichen Industriellen Revolution 
    voraus. Das Ende dieser Protoindustrialisierung ist von Land zu Land unterschiedlich, so begann die Industrielle Revolution in England 1770, in Frankreich und Belgien 1820, in der \textbf{Schweiz 1830 - 1848}, in den USA und Deutschland um 1870 und in Russland und Japan nicht vor 1880.

\subsubsection{Regionale Unterschiede}

Manche Länder waren schneller als andere. Gründe dafür sind, dass ein relativ fortgeschrittener wirtschaftlicher Entwicklungsstand sowie besondere politisch-rechtliche Bedingungen für den Übergang notwendig sind. 
    Die Industrialisierung erfolgte somit nur im nationalstaatlichen Umfeld. Es gab nicht nur Unterschiede zwischen den Ländern, auch in den Ländern gab es grosse Unterschiede.

\subsection{Industrialisierung und Weltwirtschaft}
\subsubsection{Die Industrienationen um 1900}

Die weltweite Verflechtung des Wirtschaftslebens nahm mit der Industrialisierung stark zu. Absätze fanden international statt, ebenso der Einkauf von Rohmaterialien oder Halbfabrikaten. Bis zum 1. Weltkrieg lag das 
    Zentrum der Weltwirtschaft in Europa. Die Konkurrenz zwischen den Industriemächten nahm zu, weil sie immer stärker auf ausländische Absatz- und Rohstoffmärkte angewiesen waren. Mit der Zeit gab es ein Bestreben, Besitzungen bzw. wirtschaftliche oder politische Einflusssphären aussereuropäisch zu erwerben -- Kolonien spielten also eine grosse Rolle.

\subsubsection{Die Industrienationen im internationalen Vergleich}

\begin{itemize}
    \item Lange Zeit war Grossbritannien der wirtschaftliche Führer.
    \item Das Deutsche Reich holte es an der Wende vom 19. zum 20. Jahrhundert auf.
    \item Frankreich konnte nicht mithalten, da es stagnierte.
    \item Russland wuchs nicht schnell genug, um mitzuhalten, da die Industrialisierung nur wenige Städte erfasst hatte.
    \item Europa und die USA beherrschten die Weltwirtschaft zu beginn des 20. Jahrhunderts.
\end{itemize}