\section{Die Protonenübertragung}
\subsection{Protonenspender und Protonenempfänger}
Säure-Base-Reaktionen sind Reaktionen, bei denen Protonen (\ch{H+}) von der Säure auf die Base übertragen werden. Darum nennt man diese Reaktionstyp auch \textit{Protonenübertragung} oder \textit{Protolyse}.\\
Die Reaktion beginnt mit der Spaltung des Säure-Molekül in zwei Ionen. Dieser Vorgang nennt sich \textit{Dissoziation}. Die Bindung wird \textit{heterolytisch} getrennt. D.h. das \textbf{elektronegativere Atom} erhält das \textbf{gemeinsame Elektronenpaar}.\\
Das frei werdende \ch{H+}-Ion wird an ein Wasser-Molekül gebunden. Es entsteht ein Oxonium-Ion.

\subsubsection*{Oxonium bildet H-Brücken}
Zwischen \textbf{Oxonium-Ionen} und \textbf{Wasser-Molekülen} entstehen \textbf{H-Brücken}. Durch diese H-Brücken wird das Oxonium-Ion von Wasser-Molekülen umhüllt (hydratisiert). Die von der Säure abgegebenen Protonen bleiben nicht an ein bestimmtes Wasser-Molekül gebunden, sondern wechseln mit hoher Geschwindigkeit von einem Molekül zum nächsten.

\subsection{Die Struktur von Säuren und Basen}
Aus der Brönsted-Definition ergibt sich, dass ein \textbf{Säure-Molekül} mindestens \textbf{ein \ch{H}-Atom} enthalten und es so gebunden sein muss, dass es leicht als Proton abgegeben werden kann. Dies ist aber nur der Fall, wenn die Bindung \textbf{deutlich polar} ist. Auch steigt die Tendenz ein Proton abzugeben, mit der \textbf{Bindungslänge}.\\
\textbf{Basen} müssen \textbf{Protonen binden} können. Dazu braucht es ein \textbf{freies Elektronenpaar}. Die meisten Basen sind Anionen.

\subsection{Korrespondierende Säure-Base-Paare}
Eine Säure und ihre korrespondierende Base unterscheiden sich nur durch ein Proton.\\
Protolysen sind Gleichgewichtsvorgänge zwischen zwei korrespondierenden Säure-Base-Paaren.
Die Säure \ch{HA} wird durch die Abgabe eines Protons zur korrespondierenden Base \ch{A-}. Aus der Base \ch{B-} entsteht durch die Aufnahme eines Protons die korrespondierende Säure \ch{HB}. \\
Bei der Rückreaktion gibt \ch{HB} ein Proton ab, was von \ch{A-} aufgenommen wird.

\subsection{Starke und schwache Säuren}
Die \textbf{Säurestärke} beschreibt die Tendenz, wie leicht die Säure-Moleküle ihre \textbf{Protonen abgeben}. Eine Säure ist umso \textbf{stärker}, je \textbf{leichter} und dementsprechend mehr Moleküle ihre \textbf{Protonen abgeben}.\\
Die \textbf{Säurekonstante} ($K_S$) beschreibt die Lage des Gleichgewichts bei der Protolyse in Wasser. Handelt es sich um eine \textbf{starke Säure}, liegt das Gleichgewicht \textbf{rechts}, ihr $K_S$-Wert ist \textbf{hoch}. Bei einer \textbf{schwachen Säure} liegt das Gleichgewicht \textbf{links}, ihr $K_S$-Wert ist \textbf{tief}.\\
Säuren werden zusammen mit ihrer korrespondierenden Base in der \textit{Säure-Base-Reihe} aufgelistet. Säuren stehen immer in der linken Spalte, zuoberst die stärkste, zuunterst die schwächste.
Die Säurestärke ist eine Stoffeigenschaft und darf nicht mit der Säurekonzentration oder Reaktivität verwechselt werden.

\newpage

\subsection{Säurestärke und Molekülbau}
Die Neigung einer Säure ihr Proton abzugeben, ist umso grösser je \textbf{polarer} und je \textbf{länger} die \ch{H-A}-Bindung ist. Das heisst die Säurestärke ist umso höher, je grösser und elektronegativer \ch{A} ist.\\
Innerhalb einer Gruppe\footnote{Spalte im Periodensystem} nimmt die Säurestärke nach unten zu. Die Zunahme der Bindungslänge ist entscheidend. Innerhalb einer Periode nimmt die Säurestärke nach rechts zu. Dabei spielt die Bindungslänge weniger eine Rolle, die Polarität fällt dabei stärker ins Gewicht.\\
Hat ein Säure-Atom neben dem Wasserstoff noch weitere Bindungspartner, beeinflussen diese die Polarität der \ch{H-A}-Bindung.

\subsection{Die Stärke der Base und ihrer korrespondierenden Säure}
Das Protolysengleichgewicht ist auch von der Stärke der Base abhängig. Die \textbf{Basenstärke} beschreibt die Neigung zur \textbf{Protonenaufnahme}. \textbf{Starke Basen} nehmen Protonen \textbf{leicht} auf, schwache nur schwer.\\
Die \textbf{Basenkonstante} ($K_B$) beschreibt die Lage des Gleichgewichts bei der Protolyse im Wasser und errechnet sich aus dem Anteil der Teilchen, die ein Proton aufnehmen. Der Wert der Basenkonstante ist umso \textbf{höher}, je mehr Teilchen ein Proton aufnehmen, kurz je \textbf{stärker} die Base ist.

\begin{center}
    \textbf{Eine Base ist umso stärker, je schwächer ihre korrespondierende Säure ist.}\\
    \textbf{Eine Säure ist umso stärker, je schwächer die korrespondierende Base ist.}
\end{center}

\subsection{Die Gleichgewichtslage bei Protolysen}
Das Gleichgewicht einer Protolyse liegt auf der Seite der \textbf{schwächeren Säure} und der \textbf{schwächeren Base}.\\
Das lässt sich dadurch erklären, dass die stärkere Säure ihre Protonen leicht abgibt, und dementsprechend nur noch wenige undissoziierte Moleküle vorhanden sind. Schwache Säuren geben ihre Protonen nur schwer ab, und überliegen zahlenmässig. Schwache Säuren sind reaktionsträger.\\
Ähnliches gilt für die Basen. Starke Basen nehmen leicht ein Proton auf, schwache Basen nur schwer. Schwache Basen sind demnach reaktionsträger.

\subsection{Mehrprotonige Säuren}
Mehrprotonige Säuren können mehr als ein Proton abgeben. Dabei verläuft die Abgabe stufenweise. Die Säurestärke nimmt mit jedem abgegebenen Proton ab.\\

\paragraph*{Beispiel einer mehrprotonigen Säure} anhand \ch{H2SO4}.
\begin{reactions*}
    !(stärkere~Säure)( H2SO4 ) + H2O &<=>> !(schwächere~Säure)( H3O+ ) + HSO4- \\
    !(schwächere~Säure)( HSO4- ) &<<=> !(stärkere~Säure)( SO4^{2-} )
\end{reactions*}

\subsection{Ampholyte}
\textbf{Ampholyte} sind Reaktionspartner die als \textbf{Säure oder Base} reagieren können. Sie stehen sowohl in der Säure- als auch in der Base-Reihe.\\
Ampholyte sind neben Wasser auch Wasserstoff-Ionen.