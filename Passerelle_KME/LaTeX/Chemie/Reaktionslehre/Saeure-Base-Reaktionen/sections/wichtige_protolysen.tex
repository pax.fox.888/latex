\section{Wichtige Protolysen}
\subsection{Die Neutralisation}
Bei einer Neutralisation reagiert eine \textbf{saure Lösung} mit einer \textbf{basischen Lösung}.
\paragraph*{Beispiel einer Neutralisation} anhand von Salzsäure (\ch{HCl}) und Natronlauge (\ch{NaOH}):\\
\begin{center}
    \ch{!(HCl-Lösung)( H3O+ (aq) + Cl- (aq) ) + !(NaOH-Lösung)( Na+ (aq) + OH- (aq) ) -> !(NaCl-Lösung)( Na+ (aq) + Cl- (aq) ) + !(Wasser)( 2 H2O )}
\end{center}
Durch Weglassen der \ch{Na+}- und \ch{Cl-}-Ionen, die an der Protolyse nicht teilnehmen, erhalten wir die Teilchengleichung der Neutralisation:
\begin{center}
    \ch{!(Säure)( H3O+ ) + !(Base)( OH- ) -> 2 H2O}
\end{center}

\textbf{Äquivalente Mengen} von \textbf{einprotonigen} Säuren und Laugen neutralisieren sich vollständig, da sie die gleiche Zahl von \ch{H3O+}- und \ch{OH-}-Ionen enthalten. Bei der Neutralisation von mehrprotonigen Säuren, muss die Zahl der Protonen berücksichtigt werden.

\subsection{Reaktion von Säuren mit Metallhydroxiden}
Besitzt ein Metall-Ion eine einfache Ladung (\ch{Me+}) ist es einwertig. Hat das Metall-Ion eine höhere Ladung, nennt man es mehrwertig und die Zahl der Hydroxid-Ionen ist entsprechend grösser.

\subsubsection*{Mehrprotonige Säuren und mehrwertige Hydroxide}
Bei der Neutralisation mehrprotoniger Säuren, können je nach \textbf{Mengenverhältnissen verschiedene Salze} entstehen.
\paragraph*{Beispiel Neutralisation einer mehrprotonigen Säure} anhand $1\si{\mol}$ Schwefelsäure (\ch{H2SO4}) und $1\si{\mol}$ Kaliumhydroxid (\ch{KOH}).
\begin{center}
    \ch{H2SO4 + KOH -> H2O + KHSO4}
\end{center}
Die Stoffmengen der Edukte sind zwar gleich gross aber \textbf{nicht äquivalent}. Man spricht von einer \textbf{unvollständigen Neutralisation}.\\
Da es sich bei \ch{H2SO4} um eine mehrprotonige Säure handelt, entsteht bei der Reaktion mit \ch{KOH} der Säurerest \ch{HSO4-} (Salz), der mit dem Kalium (\ch{K}) eine Bindung eingeht. Es entsteht ein Übergangsmolekül.
\begin{center}
    \ch{KHSO4 + KOH -> H2O + K2SO4} 
\end{center}
Gibt man also zu $1\si{\mol}$ Schwefelsäure (\ch{H2SO4}), $2\si{\mol}$ Kaliumhydroxid (\ch{KOH}) bei, kommt es zu einer \textbf{vollständigen Neutralisation}. Dieses Mengenverhältnis ist \textbf{äquivalent}.
\begin{center}
    \ch{H2SO4 + 2 KOH -> 2 H2O + K2SO4}
\end{center}
Zur vollständigen Neutralisation eines mehrwertigen Hydroxids, ist die entsprechend grössere Menge einer einprotonigen Säure notwendig.

\newpage

\subsubsection*{Vollständige Neutralisation}
Da eine vollständige Neutralisation voraussetzt, dass die Stoffportionen der Säure und des Hydroxids äquivalent sind, müssen die Koeffizienten entsprechend gewählt werden. 
\paragraph*{Reaktionsgleichung für vollständige Neutralisation} anhand von Schwefelsäure (\ch{H2SO4}) mit Aluminiumhydroxid (\ch{Al(OH)3}) aufstellen.\\
Ausgangsformel mit äquivalenten Mengen:
\vspace{0.5cm}
\begin{center}
    \ch{3 "\OX{h, H}" {}2SO4 + 2 Al( "\OX{oh, OH}" {})3 -> 6 H2O + ?}
    \redox(h, oh)[-stealth]{\ch{6 H+}}
\end{center}
\SI{3}{\mole} \ch{H2So4} liefern \SI{6}{\mole} \ch{H+} zur Neutralisation von \SI{6}{\mole} \ch{OH-}, dabei entstehen \SI{6}{\mole} Wasser.\\
Anhand der Koeffizienten lässt sich die Formel des Salzes herleiten:
\begin{center}
    \textcolor{darkblue}{3} \ch{H2SO4 +} \textcolor{darkgreen}{2} \ch{Al(OH)3 -> 6 H2O + Al_{\textcolor{darkgreen}{2}}(SO4)_{\textcolor{darkblue}{3}}}
\end{center}

\paragraph*{Indices einer vollständigen Neutralisation} anhand von Phosphorsäure (\ch{H3PO4}) und Calciumhydroxid (\ch{Ca(OH)2}) berechnen.\\
Provisorische Reaktionsgleichung aufstellen:
\begin{center}
    \ch{?~H3PO4 + ?~Ca(OH)2 -> ?~H2O + Ca_{?}(PO4)_{?}}
\end{center}
Nun können wir die Formel des Salzes ableiten:
\begin{center}
    \ch{Ca^{\textcolor{darkblue}{2}}+ + PO4^{\textcolor{darkgreen}{3}}- -> Ca_{\textcolor{darkgreen}{3}}(PO4)_{\textcolor{darkblue}{2}}}
\end{center}
Durch Einsetzen der Salzformel in die Reaktionsgleichung lassen sich die Koeffizienten berechnen:
\begin{center}
    \textcolor{darkblue}{2} \ch{H3PO4 +} \textcolor{darkgreen}{3} \ch{Ca(OH)2 -> ?~H2O + Ca_{\textcolor{darkgreen}{3}}(PO4)_{\textcolor{darkblue}{2}}}\par
    \vspace{0.8cm}
    \ch{2 "\OX{xh, H}" {}3PO4 + 3 Ca( "\OX{xo, OH}" {})2 ->} \textcolor{lightorange}{6} \ch{H2O + Ca3(PO4)2}
    \redox(xh, xo)[-stealth] {\textcolor{lightorange}{\ch{6 H+}}}
\end{center}

\subsubsection*{Unvollständige Neutralisation}
Sind die Stoffmengen der Säure und des Hydroxids gegeben und \textbf{nicht äquivalent}, verläuft die Neutralisation \textbf{unvollständig}.
\paragraph*{Beispiel einer unvollständigen Neutralisation} anhand \SI{1}{\mole} Phosphorsäure (\ch{H3PO4}) und \SI{1}{\mole} Calciumhydroxid (\ch{Ca(OH)2}):
\begin{center}
    \ch{1 H3PO4 + 1 Ca(OH)2 -> ?~H2O + ?}
\end{center}
Da \ch{Ca(OH)2} zweiwertig ist, gibt \ch{H3PO4} nur zwei seiner drei Protonen ab. Aus den übrigbleibenden Ionen \ch{HPO4^{2}-} und \ch{Ca^{2}+} bildet sich das Salz \ch{CaHPO4}.
\vspace{0.5cm}
\begin{center}
    \ch{1 "\OX{yh,H}" {}3PO4 + 1 Ca( "\OX{yoh, OH}" {})2 -> 2 H2O + CaHPO4}
    \redox(yh, yoh)[-stealth] {\ch{2 H+}}
\end{center}

\newpage

\subsection{Reaktionen von Säuren mit Salzen}
Salze reagieren mit Säuren die \textbf{stärker} sind, als die Säuren, welche durch die Reaktion entstehen. Anders: Bei der Reaktion mit Salzen steht die Edukt-Säure in der Säure-Base-Reihe weiter oben als die Produkt-Säure.
\begin{center}
    \ch{!(stärkere~Säure)( "\OX{h, H}" {}A ) + !(Salz)( Me "\OX{x, X}" {} ) -> !(schwächere~Säure)( HX ) + !(Salz)( MeA )}
    \redox(h, x)[-stealth]{\ch{H+}}
\end{center}
\textbf{Metalloxide} werden als \textbf{basische Oxide} bezeichnet, da das Oxid-Ion (\ch{O^{2-}}) eine sehr starke Base ist. Metalloxide reagieren mit Säuren zu einem \textbf{Salz} und \textbf{Wasser}.\\
Lösliche Metalloxide bilden mit Wasser Metallhydroxide. Die Lösung von Metallhydroxiden ist \textbf{alkalisch}.

\subsection{Reaktion von Nichtmetalloxiden mit Wasser}
Die Lösungen von Nichtmetallen in Wasser sind \textbf{sauer}. Man bezeichnet sie deswegen auch als \textit{saure Oxide}.
\begin{table}[h]
    \centering
    \caption{Nichtmetalloxide, die mit Wasser zu Säuren reagieren}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {l|c|c|c|c|c}
        Oxid & \ch{SO3} & \ch{SO2} & \ch{NO2} & \ch{P4O10} & \ch{CO2}\\
        Säure & \ch{H2SO4} & \ch{H2SO3} & \ch{HNO3}/\ch{HNO2} & \ch{H3PO4} & \ch{H2CO3}\\
    \end{tabular}
\end{table}

Die Reaktion von Salpetersäure (\ch{NO2}) ist ein Spezialfall, denn bei ihrer Reaktion mit Wasser entstehen zwei verschiedene Säuren.\footnote{Wobei \ch{HNO3} das Produkt der Reaktion von \ch{HNO2} und \ch{H2O} ist.}
\begin{center}
    \ch{2 NO2 + H2O -> !(Salpetersäure)( HNO3 ) + !(Salpetrige~Säure)( HNO2 )}
\end{center}