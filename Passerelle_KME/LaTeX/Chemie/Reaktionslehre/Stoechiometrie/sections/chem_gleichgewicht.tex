\section{Das chemische Gleichgewicht}
\subsection{Unvollständig verlaufende Reaktionen}
Viele Reaktionen verlaufen unvollständig, d.h. nicht alle Edukte reagieren miteinander. Das entstehende Reaktionsgemisch enthält am Ende Produkte und Edukte.\\
Ein \textbf{unvollständiger Stoffumsatz} kann durch eine \textbf{Hemmung} (Oxidation der Edukte) entstehen oder daran liegen, dass die \textbf{Reaktion umkehrbar} ist.
Im zweiten Fall sind die aufzufindenden Edukte keine Reste, sondern sind durch eine Gegenreaktion aus Produkten entstanden.

\subsection{Umkehrbare Reaktionen}
Viele chemische Reaktionen sind umkehrbar. Bei Gleichgewichtsvorgängen laufe die Hin- und Rückreaktionen gleichzeitig ab.
\begin{center}
    Edukte \ch{<>[Hinreaktion][Rückreaktion]} Produkte
\end{center}

\subsection{Gleichgewichtsvorgänge und Gleichgewichtsreaktionen}
Bei \textbf{Gleichgewichtsvorgängen} laufen \textbf{Hin- und Rückreaktion nebeneinander} ab. Im \textbf{Gleichgewichtszustand} laufen \textbf{Hin- und Rückreaktionen gleich häufig} ab (dynamisches Gleichgewicht)\footnote{Das Buch spricht davon, dass die Reaktionen \textit{gleich schnell} ablaufen. Es kann aber sein, dass die Reaktionen eine unterschiedliche Reaktionsdauer besitzen und sich ein dynamisches Gleichgewicht einstellt. Deswegen ist es sinnvoller von der Häufigkeit zu sprechen.}. \\
Der \textbf{Energieumsatz im Gleichgewichtszustand} ist \textbf{null} und die \textbf{Zusammensetzung des Reaktionsgemisches} bleibt \textbf{unverändert}. Die Reaktion steht scheinbar still.\\
Die Konzentrationen von Edukten und Produkten sind im Gleichgewichtszustand in der Regel nicht gleich gross. \textbf{Überwiegen} die \textbf{Produkte}, spricht man von einer \glqq \textbf{Gleichgewichtslage rechts}\grqq{}. \textbf{Überwiegen} die \textbf{Edukte} spricht man von einer \glqq \textbf{Gleichgewichtslage links}\grqq{}.\\
Ein Gleichgewichtszustand kann sich nur in einem geschlossenen System einstellen und bleibt nur in einem isolierten System erhalten.

\subsection{Gleichgewichtszustand}
Im Gleichgewichtszustand ist die Zahl der erfolgreichen Teilchenzusammenstösse für beide Reaktionen gleich gross. Die Teilchen, die weniger leicht reagieren, überwiegen zahlenmässig und stossen darum häufiger, aber selten erfolgreich zusammen. Das Gleichgewicht liegt auf der Seite der weniger reaktiven Stoffe.

\subsection{Gleichgewichtskonstante und Massenwirkungsgesetz}
Die Gleichgewichtskonstante K ist eine temperaturabhängige Konstante, welche die Lage eines Gleichgewichts angibt. Sie wird nach dem Massenwirkungsgesetz aus den Gleichgewichtskonzentrationen berechnet.\\
Für eine Reaktion der Gleichung: 
\begin{center}
    \textcolor{darkblue}{$2$}A \ch{+} \textcolor{darkblue}{$4$}B \ch{<=>} D \ch{+} \textcolor{darkblue}{$3$}E \qquad
    gilt: K$=\frac{\text{c(D)} \cdot \text{c}^{\textcolor{darkblue}{3}}(E)}{\text{c}^{\textcolor{darkblue}{2}}(A) \cdot \text{c}^{\textcolor{darkblue}{4}}(B)}$ \qquad
    c: Konzentration im Gleichgewicht
\end{center}
Der Wert muss für jede Reaktion (und jede Temperatur) experimentell ermittelt werden. Er ist umso grösser, je höher der Anteil der Produkte im Gleichgewicht ist.
\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {l l}
        $K>1$ & Gleichgewicht liegt rechts \\
        $K<1$ & Gleichgewicht liegt links
    \end{tabular}
\end{flushleft}

\subsection{Beeinflussung der Gleichgewichtslage}
\begin{center}
    \begin{tikzpicture}[every node/.style={text centered}]
        \matrix[column sep=1cm, row sep=1cm] (m) {
            \node (ZugabeEd) {\textbf{Zugabe}};
            & \node[text width = 8cm] (Hinreaktion) {Hinreaktion vorübergehend schneller als Rückreaktion \glqq Gleichgewicht verschiebt sich nach rechts\grqq{}};
            &  \node (EntnahmePr) {\textbf{Entnahme}};\\

            \node (Edukte) {\textbf{Edukte}};
            & \node[] (chemArrows) {\ch{<>}};
            & \node (Produkte) {\textbf{Produkte}};\\

            \node (EntnahmeEd) {\textbf{Entnahme}};
            & \node[text width = 8cm] (Ruekreaktion) {Rückreaktion vorübergehend schneller als Hinreaktion \glqq Gleichgewicht verschiebt sich nach links\grqq{}};
            & \node (ZugabePr) {\textbf{Zugabe}};\\
        };

        \draw[->, > = stealth, thick] (ZugabeEd) -- (Edukte);
        \draw[->, > = stealth, thick] (Edukte) -- (EntnahmeEd);
        \draw[->, > = stealth, thick] (ZugabePr) -- (Produkte);
        \draw[->, > = stealth, thick] (Produkte) -- (EntnahmePr);
    \end{tikzpicture}
\end{center}

Wird der \textbf{Gleichgewichtszustand} durch Zugabe oder Entnahme von Stoffen \textbf{gestört}, \textbf{läuft eine} der beiden \textbf{Reaktionen schneller ab}. Dies geschieht bis, der Quotient aus den \textbf{Konzentrationen wieder} dem Wert von \textbf{K entspricht}. Dieser bleibt unverändert. \\
Bei einem Gleichgewichtsvorgang begünstigt eine Temperaturerhöhung die endotherme Reaktion. Das Gleichgewicht wird zu den energiereichen Stoffen verschoben. \\
Ist die Hinreaktion exotherm, bewirkt eine Temperaturerhöhung eine Verkleinerung von K. Da die meisten Synthesen von Verbindungen exotherm verlaufen, werden sie durch eine tiefe Temperatur begünstigt. Die Temperatur kann aber nicht beliebig tief gewählt werden, weil die Elemente aktiviert werden müssen.\\
\textbf{Katalysatoren} haben \textbf{keinen Einfluss auf die Gleichgewichtslage}. Sie ermöglichen aber den Ablauf einer Reaktion bei tieferer Temperatur, wodurch sich bei den meisten exothermen Synthesen das Gleichgewicht zum Produkt verschiebt.\\
Der Druck beeinflusst die Gleichgewichtslage von Reaktionen, bei denen sich die Gesamtmenge der gasförmigen Reaktionsteilnehmer ändert. Druckerhöhung begünstigt die Reaktion, welche die Stoffmenge der Gase und damit den Druck vermindert.\\
Das Prinzip von \textbf{Le Chatelier} besagt: Zwingt man ein System im Gleichgewichtszustand durch Ändern der Bedingungen oder Eingriffe in die Zusammensetzung dazu, sich neu einzustellen, verschiebt sich die Gleichgewichtslage so, dass die \textbf{Veränderungen teilweise kompensiert} werden
