Für die Spannung an einer Spule mit der Induktivität $L$ gilt im Allgemeinen
$$ u(t)=L\frac{\mathrm{d}i(t)}{\mathrm{d}t} $$
Die Spannung entspricht also der Stromänderung pro Zeit. Folglich lässt sich 
die Spannung bei einem sinusförmigen Strom
$$ i(t)=\hat{I}\cdot \sin(\omega t)$$
als 
\begin{align*}
    u(t)&=L\cdot \frac{\mathrm{d}}{\mathrm{d}t}i(t)\\
    &= L \cdot \hat{I} \cdot \frac{\mathrm{d}}{\mathrm{d}t} \sin (\omega t)\\
    &= L \cdot \hat{I} \cdot \omega \cdot \cos (\omega t)\\
    &= L \cdot \hat{I} \cdot \omega \cdot \sin (\omega t + \frac{\pi}{2})
\end{align*}

beschreiben. Wie beim Kondensator ergibt sich wieder eine Phasenverschiebung $\varphi$ zwischen $u(t)$ und $i(t)$, 
dieses Mal jedoch von $\varphi=-\frac{\pi}{2}$. Der Strom ist um $90^{\circ}$ bzw. $\frac{\pi}{2}$ nacheilend zur Spannung.

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[   samples=100,
                        inner axis line style={thick},
                        axis lines=center,
                        axis line style={-Latex},
                        xtick={0,pi/4,pi/2,3*pi/4,pi,5*pi/4,3*pi/2,7*pi/4,2*pi},
                        ytick={-1,0,1},
                        xticklabels={0,$\frac{\pi}{4}$,$\frac{\pi}{2}$,$\frac{3\pi}{4}$,$\pi$,$\frac{5\pi}{4}$,$\frac{3\pi}{2}$,$\frac{7\pi}{4}$,$2\pi$},
                        yticklabels={,,},
                        xlabel={$t[\mathrm{s}]$},
                        ylabel={$u[\mathrm{V}]/i[\mathrm{A}]$},
                        width=10cm,
                        height=8cm,
                        xmin=0,
                        ymin=-1.5,
                        xmax=7,
                        ymax=1.5
                        ]
            
            \addplot[domain=0:6.28,blue] {sin(x*57.32)};
            \addplot[domain=0:6.28,red] {-cos(x*57.32)*0.75};
        \end{axis}

        \foreach \x/\y in {0/$0$,45/$\frac{\pi}{4}$,90/$\frac{\pi}{2}$,135/$\frac{3\pi}{4}$,180/$\pi$,225/$\frac{5\pi}{4}$,270/$\frac{3\pi}{2}$,315/$\frac{7\pi}{4}$}{
            \draw[black!50,rotate around={\x:(-3,3.21)}] (-0.76,3.21)--(-0.96,3.21);
            \node at ({-3+cos(\x)*2.64},{3.21+sin(\x)*2.64}) {\y};
        }

        \draw[black!50] (-3,3.21) circle (2.14cm);
        \fill (-3,3.21) circle (0.5mm);
        \fill[blue,rotate around={45:(-3,3.21)}] (-0.86,3.21) circle (0.5mm);
        \fill[blue] (0.94,4.72) circle (0.5mm);
        \fill[red] (0.94,2.08) circle (0.5mm);
        \draw[-Latex,blue,rotate around={45:(-3,3.21)}] (-3,3.21)--(-0.86,3.21);
        \draw[-Latex,red,rotate around={315:(-3,3.21)}] (-3,3.21)--(-1.4,3.21);
        \fill[red,rotate around={315:(-3,3.21)}] (-1.4,3.21) circle (0.5mm);
        \draw[dashed,blue] (-1.48,4.72)--(0.94,4.72);
        \draw[dashed,red] (-1.87,2.08)--(0.94,2.08);
        \node[blue] at (1.4,4.7) {$u(t)$};
        \node[red] at (1.4,2.08) {$i(t)$};
        \draw[dotted] (-3,5.35)--(1.9,5.35);
        \draw[dotted] (-3,1.07)--(5.7,1.07);
        \draw[dotted] (-3,3.21)--(3,3.21);

        \node at (-1.8,3.6) {$t$};
        \node[blue] at (-2.4,4.1) {$\vec{u}$};
        \node[red] at (-2.7,2.5) {$\vec{i}$};
        \draw[-Latex] (-1.5,3.21) arc (0:45:15mm);

        \draw[rotate around={315:(-3,3.21)}] (-2.2,3.21) arc (0:90:8mm);
        \node at (-2.5,3.21) {$\varphi$};

    \end{tikzpicture}
\end{center}

Am Zeigerdiagramm ist ersichtlich, dass (zumindest in diesem begrenzten Betrachtungsrahmen) die Induktivität das 
Gegenstück zur Kapazität darstellt. Beispielsweise lässt sich die induktive Wirkung einer Spule durch das Hinzuschalten 
eines richtig dimensionierten Kondensators bei einer bestimmten Frequenz aufheben, weil die Zeiger zueinander eine Phasenverschiebung 
von genau $\varphi=\pi$ aufweisen (siehe \ref{parallel_LC}).\\

Der Wechselstromwiderstand $X_L$ einer Spule berechnet sich aus 
$$X_L=\omega L$$
