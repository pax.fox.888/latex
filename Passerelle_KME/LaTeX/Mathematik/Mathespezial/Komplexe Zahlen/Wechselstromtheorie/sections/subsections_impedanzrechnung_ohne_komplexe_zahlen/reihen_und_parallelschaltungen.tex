\subsubsection{Reihenschaltung aus Widerstand und Induktivität (LR-Tiefpass)}\label{lr_tiefpass}
    
    \begin{center}
        \begin{tikzpicture}
            \draw (-0.5,0.25) -- (0,0.25);
            \draw (0,0) rectangle (1.5,0.5);
            \draw (1.5,0.25) -- (2.4,0.25);
            \foreach \x in {4,3.6,...,2.8}{
                \draw (\x,0.25) arc (0:180:2mm);
            }
            \draw (4,0.25) -- (4.5,0.25);
            \node at (0.75,0.75) {$R=1 \;\mathrm{k\Omega}$};
            \node at (3.25,0.75) {$L=0.1\;\mathrm{H}$};
            \draw[red,-Latex,thick] (-0.1,0.25) -- (-0.09,0.25);
            \draw[blue,-Latex,thick] (0,-0.25) -- (1.5,-0.25);
            \draw[blue,-Latex,thick] (2.5,-0.25) -- (4,-0.25);
            \node[blue] at (0.75,-0.75) {$U_R$};
            \node[blue] at (3.25,-0.75) {$U_L$};
            \node[red] at (-0.9,0.6) {$I=1\;\mathrm{mA}$};
            \node at (6,0.25){$f=1\;\mathrm{kHz}$};
        \end{tikzpicture}
    \end{center}

    Zuerst lässt sich der Wechselstromwiderstand $X_L$ berechnen.
    $$ X_L = \omega L =  2\pi \cdot 1\;\mathrm{kHz} \cdot 0.1\;\mathrm{H} \approx 628.32\;\mathrm{\Omega}$$

    Die Gesamtimpedanz $Z$ der Schaltung lässt sich nun durch vektorielles Addieren bestimmen:
    \begin{center}
        \begin{tikzpicture}
            \draw [-Latex](0,0)--(0,1.257);
            \draw [-Latex](0,0)--(2,0);
            \draw[dashed] (2,0)--(2,1.257);
            \draw[dashed] (0,1.257)--(2,1.257);
            \draw [-Latex,red](0,0)--(2,1.257);
            \node[rotate=32.12,red] at (0.8,0.78){$|Z|$};
            \node[anchor=east] at (-0.1,0.628){$|X_C|$};
            \node[anchor=north] at (1,-0.1){$|R|$};
        \end{tikzpicture}
    \end{center}
    Weil die Spannungen $\varphi=\frac{\pi}{2}$ gegeneinander phasenverschoben sind und die Wechselstromwiderstände proportional dazu sind, müssen sie 
    auch dementsprechend verrechnet werden.
    $$ Z = \sqrt{X_C^2+R^2} = \sqrt{(628.32\;\mathrm{\Omega})^2 + (1 \;\mathrm{k\Omega})^2} \approx 1.181 \;\mathrm{k\Omega}$$

    Durch den bekannten Strom $I=1\;\mathrm{mA}$ lassen sich die einzelnen Spannungen $|\vec{u}_R|$ und $|\vec{u}_L|$ berechnen.
    \begin{align*}
        |\vec{u}_R|&=R\cdot I=1 \;\mathrm{k\Omega} \cdot 1\;\mathrm{mA} = 1\;\mathrm{V}\\
        |\vec{u}_L|&=X_L\cdot I=628.32\;\mathrm{\Omega} \cdot 1\;\mathrm{mA} = 628.32\;\mathrm{mV}
    \end{align*}
    Die Spannungen sind aber phasenverschoben gegeneinander und sind darum als vektorielle Grössen zu betrachten, weshalb die Beträge
    nicht einfach addiert werden können.\\
  
    Die Situation lässt sich wie folgt in einem Zeigerdiagramm veranschaulichen:
    \begin{center}
        \begin{tikzpicture}
            
            \foreach \x/\y in {0/$0$,45/$\frac{\pi}{4}$,90/$\frac{\pi}{2}$,135/$\frac{3\pi}{4}$,180/$\pi$,225/$\frac{5\pi}{4}$,270/$\frac{3\pi}{2}$,315/$\frac{7\pi}{4}$}{
                \draw[black!50,rotate around={\x:(0,0)}] (2.04,0)--(2.24,0);
                \node at ({0+cos(\x)*2.64},{0+sin(\x)*2.64}) {\y};
            }
    
            \draw[black!50] (0,0) circle (2.14cm);
            \fill (0,0) circle (0.5mm);
            \fill[violet,rotate around={90:(0,0)}] (1.2,0) circle (0.5mm);
            \fill[blue,rotate around={0:(0,0)}] (1.6,0) circle (0.5mm);
            \fill[red,rotate around={0:(0,0)}] (2.14,0) circle (0.5mm);
            \draw[-Latex,violet,rotate around={90:(0,0)}] (0,0)--(1.2,0);
            \draw[dashed,violet] (1.6,0)--(1.6,1.2);
            \draw[-Latex,blue,rotate around={0:(0,0)},thick] (0,0)--(1.6,0);
            \draw[dashed,blue] (0,1.2)--(1.6,1.2);
            \draw[-Latex,red,rotate around={0:(0,0)}] (0,0)--(2.14,0);
            \draw[-Latex] (0,0)--(1.6,1.2);
            
            \node[violet] at (-0.3,0.6){$\vec{u}_L$};
            \node[blue] at (0.8,-0.3){$\vec{u}_R$};
            \node[red] at (1.9,-0.3){$\vec{i}$};
            \node[rotate=40] at (0.7,0.8){$\vec{u}_{\mathrm{ges}}$};
    
        \end{tikzpicture}
    \end{center}

    $\vec{u}_R$ liegt auf dem Zeiger $\vec{i}$, weil nach dem ohmschen Gesetz die Spannung an einem Widerstand stets proportional zum Strom ist.
    $\vec{u}_L$ ist $\varphi=\frac{\pi}{2}$ gegen $\vec{u}_R$ phasenverschoben.
    Die Gesamtspannung am LR-Glied lässt sich direkt aus der Vektoraddition 
    $\vec{u}_{\mathrm{ges}} = \vec{u}_R + \vec{u}_L$ ablesen.
    Der Betrag der Gesamtspannung errechnet sich aus
    \begin{align*}
        |\vec{u}_{\mathrm{ges}}| &= \sqrt{|\vec{u}_R|^2+|\vec{u}_L|^2}\\
        &= \sqrt{(1\;\mathrm{V})^2+(628.32\;\mathrm{mV})^2}\\
        &= 1.181\;\mathrm{V}
    \end{align*}

    \subsubsection{Parallelschaltung von Kondensator und Spule (Parallelschwingkreis)}\label{parallel_LC}

    \begin{center}
        \begin{tikzpicture}
            \draw (-0.9,0) -- (-0.4,0);
            \foreach \x in {0,0.4,...,1.2}{
                \draw (\x,0) arc (0:180:2mm);
            }
            \draw (1.2,0) -- (1.7,0);
            \draw (0.3,-0.4)--(0.3,-1.2);
            \draw (0.5,-0.4)--(0.5,-1.2);
            \draw (0.3,-0.8)--(-0.9,-0.8);
            \draw (0.5,-0.8)--(1.7,-0.8);


            \draw (-0.9,0)--(-0.9,-0.8);
            \draw (1.7,0)--(1.7,-0.8);

            \fill (-0.9,-0.4) circle (0.5mm);
            \fill (1.7,-0.4) circle (0.5mm);

            \draw (-2,-0.4)--(-0.9,-0.4);
            \draw (2.8,-0.4)--(1.7,-0.4);

            \draw[-Latex,red,thick] (-0.6,0)--(-0.5,0);
            \draw[-Latex,red,thick] (-0.6,-0.8)--(-0.5,-0.8);
            \draw[-Latex,red,thick] (-1.6,-0.4)--(-1.5,-0.4);

            \node[red] at (-0.6,0.3) {$I_L$};
            \node[red] at (-0.6,-1.1) {$I_C$};
            \node[red] at (-1.6,-0.1) {$I_{\mathrm{ges}}$};
            \draw[blue,-Latex,thick] (-0.9,-1.5)--(1.7,-1.5);
            \node[blue] at (0.4,-1.8) {$U$};
        \end{tikzpicture}
    \end{center}

    In diesem Fall ist die Spannung $U$ die gemeinsame Grösse. Sie wird deshalb im Zeigerdiagramm bei $\varphi=0$ eingezeichnet.
    Die Ströme $I_L$ und $I_C$ sind jeweils um $-\frac{\pi}{2}$ resp. $+\frac{\pi}{2}$ gegen diese Spannung phasenverschoben.
    
    \begin{center}
        \begin{tikzpicture}
            
            \foreach \x/\y in {0/$0$,45/$\frac{\pi}{4}$,90/$\frac{\pi}{2}$,135/$\frac{3\pi}{4}$,180/$\pi$,225/$\frac{5\pi}{4}$,270/$\frac{3\pi}{2}$,315/$\frac{7\pi}{4}$}{
                \draw[black!50,rotate around={\x:(0,0)}] (2.04,0)--(2.24,0);
                \node at ({0+cos(\x)*2.64},{0+sin(\x)*2.64}) {\y};
            }
    
            \draw[black!50] (0,0) circle (2.14cm);
            \fill (0,0) circle (0.5mm);
            \fill[red,rotate around={90:(0,0)}] (1.2,0) circle (0.5mm);
            \fill[red,rotate around={270:(0,0)}] (1.2,0) circle (0.5mm);
            \fill[blue,rotate around={0:(0,0)}] (2.14,0) circle (0.5mm);
            \draw[-Latex,red,rotate around={90:(0,0)}] (0,0)--(1.2,0);
            \draw[-Latex,red,rotate around={270:(0,0)}] (0,0)--(1.2,0);
            \draw[-Latex,blue,rotate around={0:(0,0)}] (0,0)--(2.14,0);
            
            \node[red] at (-0.3,0.6){$\vec{i}_C$};
            \node[blue] at (0.8,-0.3){$\vec{u}$};
            \node[red] at (-0.3,-0.6){$\vec{i}_L$};
    
        \end{tikzpicture}
    \end{center}

    Aus dem Diagramm wird ersichtlich, dass die Ströme von Kondensator und Spule in diesem \textit{Schwingkreis} hin- und herpendeln 
    und sich von aussen gesehen aufheben. Ist die Frequenz so gross, dass gilt $X_L=X_C$, dann sind auch die Ströme $I_L$ und $I_C$ gleich gross 
    und zeitlich so gegeneinander verschoben, dass sie sich exakt aufheben. Der Parallelschwingkreis hat bei dieser Frequenz eine (theoretisch) unendlich grosse
    Impedanz $Z$.\\

    Aus dem Zeigerdiagramm ergibt sich der Zusammenhang:
    $$\vec{i}_{\mathrm{ges}} = \vec{i}_C + \vec{i}_L$$
    Das Dividieren der Gleichung mit der Spannung führt zum Zusammenhang der Wechselstromwiderstände:
    \begin{align*}
    \frac{\vec{i}_{\mathrm{ges}}}{\vec{u}} &= \frac{\vec{i}_C}{\vec{u}} + \frac{\vec{i}_L}{\vec{u}}\\
    \frac{1}{Z} &= \frac{1}{X_C} - \frac{1}{X_L}\\
    \frac{1}{Z} &= \omega C - \frac{1}{\omega L}\\
    \frac{1}{Z} &= \frac{\omega C \omega L}{\omega L} - \frac{1}{\omega L}\\
    Z &= \frac{\omega L}{\omega^2 CL-1}
    \end{align*}

    Stellt man nun die Impedanz $Z (\omega)$ als Funktion dar, erhält man die untenstehende, für einen Parallelschwingkreis charakteristische, Kurve:

    \begin{center}
        \begin{tikzpicture}
            \begin{axis}[   samples=100,
                            inner axis line style={thick},
                            axis lines=center,
                            axis line style={-Latex},
                            xticklabels={,,},
                            yticklabels={,,},
                            xlabel={$\omega\;[s^{-1}]$},
                            ylabel={$Z\;[\mathrm{\Omega}]$},
                            ymax=15,
                            ymin=-1,
                            xmin=-1,
                            xmax=15,
                            width=8cm,
                            height=8cm
                            ]
            \addplot[domain=0.7:11,blue!50] {1/(0.13*x)};
            \addlegendentry{$X_C$}
            \addplot[domain=0:15,red!50] {0.5*x};
            \addlegendentry{$X_L$}
            \addplot[domain=0:11,black,thick] {abs((x*0.5)/(x^2*0.5*0.13-1))};
            \addlegendentry{$Z$}

            

            \draw[dashed] (axis cs:3.922,0)--(axis cs:3.922,15);
    
            \end{axis}
        \end{tikzpicture}
    \end{center}

    Die Frequenz $f$, an welcher die Impedanz ihr Maximum erreicht, wird \textit{Resonanzfrequenz} genannt.
    
    