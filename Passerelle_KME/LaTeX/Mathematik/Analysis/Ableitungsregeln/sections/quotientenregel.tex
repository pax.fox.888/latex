% https://de.wikipedia.org/wiki/Quotientenregel#Herleitung

\subsection{Definition}
    Ist $f(x)$ darstellbar als Quotient zweier differenzierbarer Funktionen $u(x)$ und $v(x)$ mit $v(x)\neq 0$, so dass gilt:
    $$ f(x) = \frac{u(x)}{v(x)} $$
    lässt sich die Ableitung wie folgt beschreiben:
    $$ f'(x) = \left(\frac{u(x)}{v(x)}\right)' = \frac{u'(x) \cdot v(x) - v'(x) \cdot u(x)}{(v(x))^2} $$
    oder in Kurzform:
    $$ f'(x) = \frac{u'v-v'u}{v^2} $$

\subsection{Beispiel}
    Gesucht sei die Ableitung der Funktion $f(x)=\frac{x^3-2x+6}{x-1}$. Die Funktion $f(x)$ lässt sich 
    aufspalten in $u(x)=x^3-2x+6$ und $v(x)=x-1$ mit den zugehörigen Ableitungen nach Exponentenregel:
    $u'(x)=3x^2-2$ und $v'(x)=1$. Gemäss der Quotientenregel folgt die Ableitung $f'(x)$:
   \begin{align*}
    \underline{\underline{f'(x)}} &= \frac{u'(x) \cdot v(x) - v'(x) \cdot u(x)}{(v(x))^2} \\
    &= \frac{(3x^2-2) \cdot (x-1) - 1 \cdot (x^3-2x+6)}{(x-1)^2} \\
    &= \frac{3x^3-3x^2\cancel{-2x}+2-x^3\cancel{+2x}-6}{x^2+2x+2} \\
    &= \underline{\underline{\frac{2x^3-3x^2-4}{x^2+2x+2}}}
   \end{align*} 

\subsection{Herleitung}
   Der Quotient $f(x)=\frac{u(x)}{v(x)}$ lässt sich als Steigung $\frac{\Delta y}{\Delta x}$ eines Steigungsdreiecks interpretieren. 
   \begin{center}
    \begin{tikzpicture}
        \begin{axis}[   samples=100,
                        inner axis line style={thick},
                        axis lines=center,
                        axis line style={-Latex},
                        xticklabels={,,},
                        yticklabels={,,},
                        xlabel={$v$},
                        ylabel={$u$},
                        ymax=3.5,
                        ymin=-1,
                        xmin=-1,
                        xmax=3.5,
                        width=8cm,
                        height=8cm
                        ]
        

            \fill[blue, fill opacity=0.5] (axis cs:0,0)--(axis cs:2,2)--(axis cs: 2,0)--cycle;
            \path[draw,thick] (axis cs:0,0)--(axis cs:2,2) node[midway, above, sloped]{$m_0=\frac{u(x)}{v(x)}$};
            \draw[dashed] (axis cs:0,2)--(axis cs:3,2);

            \fill[blue, fill opacity=0.3] (axis cs:0,0)--(axis cs:3,2.5)--(axis cs: 3,0)--cycle;
            \path[draw,thick] (axis cs:0,0)--(axis cs:3,2.5) node[midway, below, sloped]{$m_\Delta=\frac{u(x)+\Delta u(x)}{v(x)+\Delta v(x)}$};
            \draw[dashed] (axis cs:0,2.5)--(axis cs:3,2.5);

            \node at (axis cs: 1,-0.3){\Large{$\underbrace{\phantom{XXXXXX}}_{v(x)}$}};
            \node[rotate=270] at (axis cs: -0.4,1){$\underbrace{\phantom{XXXXXXXXI}}_{\rotatebox{90}{$u(x)$}}$};

            \node at (axis cs: 2.5,-0.3){\Large{$\underbrace{\phantom{XXX}}_{\Delta v(x)}$}};
            \node[rotate=270] at (axis cs: -0.5,2.25){$\underbrace{\phantom{X}}_{\rotatebox{90}{$\Delta u(x)$}}$};

        \end{axis}
    \end{tikzpicture}
\end{center}

Die Frage ist also, wie stark sich die Steigung eines Dreiecks ändert, wenn sich die Katheten um jeweils einen gewissen Betrag $\Delta u(x)$ 
resp. $\Delta v(x)$ ändern.\\

Die Änderung der Steigung lässt sich wie folgt formulieren:
\begin{align*}
    \Delta m = \Delta \left(\frac{u(x)}{v(x)}\right) &= \frac{u(x)+\Delta u(x)}{v(x)+\Delta v(x)} - \frac{u(x)}{v(x)}\\
    &= \frac{v(x) \cdot (u(x)+\Delta u(x)) - u(x) \cdot (v(x)+\Delta v(x))}{v(x)\cdot(v(x)+\Delta v(x))}\\
    &= \frac{v(x) \cdot \Delta u(x) - u(x) \cdot \Delta v(x)}{v(x)^2 + (v(x) \cdot \Delta v(x))}
\end{align*}

Dividiert man nun beide Seiten durch $\Delta x$, ergeben sich mit $\lim\limits_{\Delta x \rightarrow 0}$ die Ableitungen der Funktionen 
$u(x)$ und $v(x)$ und somit die gesuchte Quotientenregel.

\begin{align*}
    \frac{\Delta \left(\frac{u(x)}{v(x)}\right)}{\Delta x} &= \frac{v(x) \cdot \frac{\Delta u(x)}{\Delta x} - u(x) \cdot \frac{\Delta v(x)}{\Delta x}}{v(x)^2 + (v(x) \cdot \Delta v(x))}\\
    &= \boxed{\frac{v(x)\cdot u'(x) - u(x) \cdot v'(x)}{v(x)^2}}
\end{align*}
$\blacksquare$.