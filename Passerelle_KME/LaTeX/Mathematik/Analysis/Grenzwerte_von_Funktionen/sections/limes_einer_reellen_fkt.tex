$\lim\limits_{x \rightarrow p}{f(x)}$ bezeichnet den Grenzübergang (Grenzwert) der Funktion $f(x)$
        der Variable $x$ gegen $p$. Dabei kann $p$ sowohl einen reellen Wert, als auch die 
        symbolischen Werte $\infty$ bzw. $-\infty$ annehmen. Bei endlichen Werten muss
        $p$ nicht im Definitionsbereich von $f(x)$ liegen.

        \begin{center}
            \begin{tikzpicture}
                \begin{axis}[   samples=100,
                                inner axis line style={thick},
                                axis lines=center,
                                axis line style={-Latex},
                                ymax=1.2,
                                ymin=-0.4,
                                ytick=1,
                                ]
                \addplot[domain=-8:-0.01,blue] {sin(deg(x-2))/(x-2)};
                \addplot[domain=0.01:8,blue] {sin(deg(x-2))/(x-2)};
                \addplot[holdot] coordinates{(2,1)};

                \addplot[mark=none, black, dotted] coordinates {(2,0) (2,1)};
                \addplot[mark=none, black, dotted] coordinates {(0,1) (2,1)};

                \node at (axis cs:2, -0.2){$p$};
                \node at (axis cs:-3, 1){$\lim\limits_{x \rightarrow p}{f(x)}$};

                \end{axis}
            \end{tikzpicture}
        \end{center}

        Betrachtet man dazu den Definitionsbereich $D_f = \{x \in \mathbb{R} \;|\; x \neq 2\}$ der 
        Funktion $f(x)=\frac{\sin(x-2)}{x-2}$ erkennt man, dass wir mit dem Limes ein Werkzeug besitzen,
        womit wir den Punkt $(2,1)$ \textit{beliebig genau} annähern können, auch wenn $x=2$ nicht direkt
        in die Funktion eingesetzt werden darf.
    
        \subsection{Argument endlich, Grenzwert endlich}
            Ist sowohl $p$ wie auch $\lim\limits_{x \rightarrow p}{f(x)} = r$ endlich, lässt sich das anschaulich mit einer ($\epsilon$, $\delta$)-Definition\footnote{Siehe \textit{Epsilontik.}} zeigen.

                \begin{center}
                    \begin{tikzpicture}
                        \begin{axis}[   samples=100,
                                        inner axis line style={thick},
                                        axis lines=center,
                                        axis line style={-Latex},
                                        xticklabels={,,},
                                        yticklabels={,,},
                                        xmin=-1,
                                        ymin=-2
                                        ]
                        \addplot[domain=1:1.428,blue] {10/x};
                        \addplot[domain=1.429:3.332,blue,very thick] {10/x};
                        \addplot[domain=3.333:6,blue] {10/x};

                        \addplot[mark=none, black, thick, dotted] coordinates {(2,0) (2,5)};
                        \addplot[mark=none, black, thick, dotted] coordinates {(0,5) (2,5)};

                        \addplot[mark=none, black, dotted] coordinates {(0,7) (1.429,7)};
                        \addplot[mark=none, black, dotted] coordinates {(0,3) (3.333,3)};
                        \addplot[mark=none, black, dotted] coordinates {(1.429,0) (1.429,7)};
                        \addplot[mark=none, black, dotted] coordinates {(3.333,0) (3.333,3)};

                        \node at (axis cs:-0.3, 5){$r$};
                        \node at (axis cs:2, -1.2){$p$};

                        \node at (axis cs:0.3, 6){$\bigg\}\epsilon$};
                        \node at (axis cs:0.3, 4){$\bigg\}\epsilon$};

                        \node at (axis cs:1.75, -0.4){$\delta$};
                        \node at (axis cs:2.25, -0.4){$\delta$};
                        \node at (axis cs:3.2, 9){$\delta$-Streifen};

                        \addplot[smldot] coordinates{(2,5)};
                        \addplot[smldot] coordinates{(1.429,7)};
                        \addplot[smldot] coordinates{(3.333,3)};

                        \begin{pgfonlayer}{bg}
                            \fill [black!10] (axis cs:1.6,0) rectangle (axis cs:2.4,10);
                        \end{pgfonlayer}
                    
                        \end{axis}
                    \end{tikzpicture}
                \end{center}

            Der \textbf{beidseitige} Grenzwert existiert an der Stelle $p$ nur genau dann, wenn zu jedem $\epsilon > 0$ ein $\delta > 0$ existiert, sodass für alle $p-\delta < x < p+\delta$ auch $|f(x)-r|<\epsilon$ gilt. 
            
            Für jedes noch so kleine $\epsilon$ muss also ein $\delta$-Streifen existieren, bei dem alle $x$-Werte innerhalb 
            dieses $\delta$-Streifens einen Funktionswert innerhalb des $\epsilon$-Streifens besitzen (Werte dick gezeichnet).\\
            
            So lässt sich auch direkt die \textbf{Stetigkeit} einer Funktion definieren. Anschaulich bedeutet es für eine Funktion stetig zu sein, dass der Funktionsgraph
            keine Definitionslücken oder Sprünge aufweist.\\

            Betrachten wir nun eine Funktion, die keinen beidseitigen Grenzwert besitzt.
            \begin{center}
                \begin{tikzpicture}
                    \begin{axis}[   samples=100,
                                    inner axis line style={thick},
                                    axis lines=center,
                                    axis line style={-Latex},
                                    xticklabels={,,},
                                    yticklabels={,,},
                                    xmin=-1,
                                    ymin=-2
                                    ]
                    \addplot[domain=0:1,blue] {x};
                    \addplot[domain=1:2,blue, very thick] {x};
                    \addplot[domain=2:2.4,red, very thick] {x+2};
                    \addplot[domain=2.4:6,blue] {x+2};
                    \addplot[soldot] coordinates {(2,4)};
                    \addplot[holdot] coordinates {(2,2)};

                    \addplot[mark=none, black, thick, dotted] coordinates {(2,0) (2,2)};
                    \addplot[mark=none, black, thick, dotted] coordinates {(0,2) (2,2)};

                    \addplot[mark=none, black, dotted] coordinates {(0,1) (1,1)};
                    \addplot[mark=none, black, dotted] coordinates {(0,3) (6,3)};
                    \addplot[mark=none, black, dotted] coordinates {(1,0) (1,1)};

                    \node at (axis cs:-0.5, 2){$r$};
                    \node at (axis cs:2, -1.2){$p$};

                    \node at (axis cs:0.2, 1.5){$\big\}\epsilon$};
                    \node at (axis cs:0.2, 2.5){$\big\}\epsilon$};

                    \node at (axis cs:1.75, -0.4){$\delta$};
                    \node at (axis cs:2.25, -0.4){$\delta$};
                    \node at (axis cs:3.2, 7.5){$\delta$-Streifen};

                    \begin{pgfonlayer}{bg}
                        \fill [black!10] (axis cs:1.6,0) rectangle (axis cs:2.4,8);
                    \end{pgfonlayer}

                    \addplot[smldot] coordinates{(1,1)};
                
                    \end{axis}
                \end{tikzpicture}
            \end{center}
            Hier ist ersichtlich, dass sich der $\epsilon$-Streifen klein genug wählen lässt, so dass kein $\delta$-Streifen mehr gefunden werden kann, bei dem 
            \textit{alle} $x$-Werte darin einen Funktionswert besitzen, der noch innerhalb des $\epsilon$-Streifen liegt.\\

            Erlaubt man es nun, nur die Werte $f(x) + \epsilon$ und  $x + \delta$ bzw. $f(x) - \epsilon$ und  $x - \delta$ zu berücksichtigen,
            kann man \textbf{einseitige Grenzwerte} definieren. Hierbei ist es jedoch erforderlich anzugeben, von welcher Seite man sich $p$ annähert mit 
            $\lim\limits_{x \rightarrow p^+}{f(x)}$ resp. $\lim\limits_{x \rightarrow p^-}{f(x)}$.
        
        \subsection{Argument endlich, Grenzwert unendlich}
            Es gilt $\lim\limits_{x \rightarrow p}{f(x)} = \infty$, wenn zu jeder (noch so grossen) Zahl $T$ ein (noch so kleiner) $\delta$-Streifen existiert,
            sodass für alle $x$ mit $p-\delta < x < p+\delta$ (innerhalb des $\delta$-Streifens) gilt, dass $f(x)>T$.
        
            \begin{center}
                \begin{tikzpicture}
                    \begin{axis}[   samples=100,
                                    inner axis line style={thick},
                                    axis lines=center,
                                    axis line style={-Latex},
                                    xticklabels={,,},
                                    yticklabels={,,},
                                    xmin=-0.3,
                                    xmax=1.7,
                                    ymin=-1,
                                    ymax=12
                                    ]
                    \addplot[domain=0.51:3,blue] {1/(x-0.5)};

                    \addplot[mark=none, black, dotted] coordinates {(0,3) (0.833,3)};
                    \addplot[mark=none, black, dotted] coordinates {(0,6) (0.667,6)};
                    \addplot[mark=none, black, dotted] coordinates {(0.,10) (0.6,10)};

                    \node at (axis cs:-0.1, 3){$T_1$};
                    \node at (axis cs:-0.1, 6){$T_2$};
                    \node at (axis cs:-0.1, 10){$T_3$};


                    \node [style={black!40}] at (axis cs:0.45, -0.5){$\delta_3$};
                    \node [style={black!30}]at (axis cs:0.35, -0.5){$\delta_2$};
                    \node [style={black!20}]at (axis cs:0.25, -0.5){$\delta_1$};

                    \node at (axis cs:1.15, 11.5){$\delta_n$-Streifen};

                
                    \begin{pgfonlayer}{l3}
                        \fill [black!10] (axis cs:0.167,0) rectangle (axis cs:0.833,12);
                    \end{pgfonlayer}

                    \begin{pgfonlayer}{l2}
                        \fill [black!20] (axis cs:0.333,0) rectangle (axis cs:0.667,12);
                    \end{pgfonlayer}

                    \begin{pgfonlayer}{l1}
                        \fill [black!30] (axis cs:0.4,0) rectangle (axis cs:0.6,12);
                    \end{pgfonlayer}

                    \addplot[mark=none, black, thick, dotted] coordinates {(2,0) (2,12)};

                    \addplot[mark=none, black, dashed] coordinates {(0.5,0) (0.5,12)};

                    \addplot[smldot] coordinates{(0.833,3)};
                    \addplot[smldot] coordinates{(0.667,6)};
                    \addplot[smldot] coordinates{(0.6,10)};

                    \end{axis}
                \end{tikzpicture}
            \end{center}

            Man nennt solche Funktionen $f$ für $x$ gegen $p$ \textbf{bestimmt divergent.}
        
        \subsection{Argument unendlich, Grenzwert endlich}

            \begin{center}
                \begin{tikzpicture}
                    \begin{axis}[   samples=200,
                                    inner axis line style={thick},
                                    axis lines=center,
                                    axis line style={-Latex},
                                    xticklabels={,,},
                                    yticklabels={,,},
                                    ymax=4,
                                    ymin=-0.5,
                                    xmin=-0.5
                                    ]
                    

                    \addplot[domain=0.5:3.595,blue] {sin(deg(x*4))*(4/x^2)+2};
                    \addplot[domain=3.595:8,blue,very thick] {sin(deg(x*4))*(4/x^2)+2};

                    \addplot[mark=none, black, dashed] coordinates {(0,2) (8,2)};

                    \node at (axis cs:-3, 1){$\lim\limits_{x \rightarrow p}{f(x)}$};


                    \begin{pgfonlayer}{bg}
                        \fill [black!10] (axis cs:0,1.7) rectangle (axis cs:8,2.3);
                    \end{pgfonlayer}

                    \addplot[smldot] coordinates{(3.595,2.3)};
                    \addplot[mark=none, black, dotted] coordinates {(3.595,2.3) (3.595,0)};


                    \node at (axis cs:0.3, 2.15){$\}\epsilon$};
                    \node at (axis cs:0.3, 1.85){$\}\epsilon$};

                    \node at (axis cs:-0.3, 2){$r$};
                    \node at (axis cs:3.595, -0.2){$\epsilon_0$};

                    \end{axis}
                \end{tikzpicture}
            \end{center}

            Es gilt $\lim\limits_{x \rightarrow \infty}{f(x)} = r$, wenn für beliebig kleine $\epsilon$ ab einer gewissen Eintauchstelle $\epsilon_0$ in den $\epsilon$-Streifen für alle 
            folgenden $x>\epsilon_0$ gilt, dass $|f(x)-r| < \epsilon$.
            Man nennt solche Funktionen $f$ für $x$ gegen $\infty$ \textbf{konvergent.}
