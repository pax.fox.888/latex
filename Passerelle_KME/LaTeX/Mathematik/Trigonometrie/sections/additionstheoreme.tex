\section{Additionstheoreme}
Will man die Sinus- oder Cosinus-Werte von zwei Winkeln addieren, muss man die sog. Additionstheoreme anwenden. Denn:
\begin{align*}
    30\degree + 60\degree &= 90\degree \qquad \sinpar{90\degree} = 1 \qquad \cospar{90\degree} = 0\\
    \sinpar{30\degree} + \sinpar{60\degree} &= \frac{1 + \sqrt{3}}{2} \neq 1\\
    \cospar{30\degree} + \cospar{60\degree} &= \frac{1 + \sqrt{3}}{2} \neq 1
\end{align*}

Gesucht ist also eine Methode, um von den Funktionswerten $\sinpar{\alpha}$, $\sinpar{\beta}$  und $\cospar{\alpha}$, $\cospar{\beta}$ 
für zwei beliebige Winkel $\alpha$ und $\beta$ auf die Funktionswerte für 
$\sinpar{\alpha+\beta}$ und $\cospar{\alpha+\beta}$ zu schliessen. Dieses Werkzeug liefern die Additionstheoreme.

\subsection{Additionstheorem Sinus}
\begin{equation*}
    \boxed{\sinpar{\alpha \pm \beta} = \sinpar{\alpha} \cdot \cospar{\beta} \pm \sinpar{\beta} \cdot \cospar{\alpha}}
\end{equation*}
\begin{center}
    \begin{tikzpicture}
        \begin{axis}[
            axis lines = middle,
            axis equal = true,
            xticklabels={,,},
            yticklabels={,,},
            xmin = -0.1,
            xmax = 1.1,
            ymin = -0.1,
            ymax = 1.1,
            height = 8cm,
            width = 8cm,
        ]
    
        \path (axis cs: 0, 0) coordinate (origin);
        \path (axis cs: 1, 0) coordinate (xAxisAt1);
        \draw[gray!30] let \p1=(origin),\p2=(xAxisAt1), \n1={veclen(\x2-\x1,\y2-\y1)} in (origin) circle[radius=\n1];      
    
        \coordinate (point1) at (axis cs: 0.5, 0.866);
        \coordinate (point1Base) at (axis cs: 0.5, 0);
        \coordinate (point2) at (axis cs: 0.95, 0.312);
        \coordinate (point2Base) at (axis cs: 0.95, 0);
        \coordinate (point3) at (axis cs: 0.7, 0.2299);
        \coordinate (point4) at (axis cs: 0.5, 0.2299);
        \coordinate (point5) at (axis cs: 0.7, 0.866);
        \coordinate (point5Base) at (axis cs: 0.7, 0);
        
        \draw[] (axis cs: 0.7, 0) -- (point3) -- (point1);
    
        \pic[draw=darkblue, angle radius=1cm] {angle=point2--origin--point1};
        \node[darkblue] at (axis cs: 0.18, 0.15) {\scriptsize $\beta$};
        \pic[draw=darkgreen, angle radius=1cm] {angle=point1Base--origin--point2};
        \node[darkgreen] at (axis cs: 0.22, 0.03) {\scriptsize $\alpha$};

        \draw[darkblue, thick] (origin) -- (point1) -- (point3) -- cycle;
        \path (origin) -- (point3) node[above, midway, darkblue, sloped] {$\cospar{\beta}$};
        \path (point3) -- (point1) node[below, midway, sloped, darkblue] {$\sinpar{\beta}$};

        \draw[] (point1) -- (point5) -- (axis cs:0.7,0.2299) -- cycle;
        \pic[draw=darkgreen, angle radius=1cm] {angle=point5--point3--point1};
        \node[darkgreen] at (axis cs: 0.67, 0.45) {\scriptsize $\alpha$};

        \draw[darkgreen, thick] (origin) -- (point2) -- (point2Base) -- cycle;
        \path (point2Base)--(point2) node[darkgreen, midway, below, sloped] {$\sinpar{\alpha}$};
        \path (origin)--(point2Base) node[darkgreen, midway, below, sloped] {$\cospar{\alpha}$};

        \draw[thick, lightorange] (point5Base) -- (point5);
        \path (point5Base) -- (point5) node[below,midway,lightorange,sloped] {$\sinpar{\alpha+\beta}$};

        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label=right:{$D$}] at (point2){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label={$C$}] at (point1){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label={$B$}] at (point5){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label={[anchor=center,label distance=3mm] 330:$A$}] at (point3){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label=below:{$E$}] at (point5Base){};


        \end{axis}
    \end{tikzpicture}
\end{center}

Gegeben seien zwei Dreiecke (\textcolor{darkblue}blau und \textcolor{darkgreen}{grün}) am Einheitskreis mit den jeweiligen Winkeln 
am Ursprung $\alpha$ und $\beta$. Das \textcolor{darkblue}blaue Dreieck sei zudem um den Ursprung um $\alpha$ rotiert worden, sodass es 
nun \glqq auf\grqq{} dem \textcolor{darkgreen}{grünen} Dreieck zu liegen kommt. Durch die Rotation des Dreiecks wird auch die (zuvor senkrechte)
Strecke $\overline{AC}$ um $\alpha$ rotiert. Sie weicht nun also um diesen Winkel von der (senkrechten) Strecke $\overline{AB}$ ab. Folglich 
entspricht $\angle BAC = \alpha$.\\

Die \textcolor{lightorange}{orange} eingefärbte Strecke entspricht der Y-Koordinate von Punkt $C$ und somit dem gesuchten Funktionswert $\sinpar{\alpha+\beta}$.
Die Distanz $\overline{AB}$ bildet die Ankathete des Dreiecks $\triangle ABC$. Die Hypotenuse dieses Dreiecks entspricht $\sinpar{\beta}$.
Daraus folgt $\cospar{\alpha}=\frac{\textrm{Ank.}}{\textrm{Hyp.}}=\frac{\overline{AB}}{\sinpar{\beta}} \implies \overline{AB} = \cospar{\alpha}\sinpar{\beta}$.\\

Die Distanz $\overline{AE}$ bildet die Gegenkathete des Dreiecks $\triangle OEA$. Die Hypotenuse dieses Dreiecks entspricht $\cospar{\beta}$.
Daraus folgt $\sinpar{\alpha}=\frac{\textrm{Geg.}}{\textrm{Hyp.}}=\frac{\overline{AE}}{\cospar{\beta}} \implies \overline{AE} = \sinpar{\alpha}\cospar{\beta}$.\\

Nun lässt sich erkennen: $\sinpar{\alpha+\beta} = \overline{AE}+\overline{AB} = \sinpar{\alpha}\cospar{\beta} + \cospar{\alpha}\sinpar{\beta}$.
Das unterschiedliche Vorzeichen in der verallgemeinerten Form lässt sich dadurch erklären, dass man sich die Addition mit einem \glqq negativen\grqq{}
Winkel $\beta$ vorstellt. Dabei \glqq kippt\grqq{} das \textcolor{darkblue}blaue Dreieck wieder in das \textcolor{darkgreen}{grüne}.
\subsection{Additionstheorem Cosinus}
\begin{equation*}
    \boxed{\cospar{\alpha \pm \beta} = \cospar{\alpha} \cdot \cospar{\beta} \mp \sinpar{\alpha} \cdot \sinpar{\beta}}
\end{equation*}

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[
            axis lines = middle,
            axis equal = true,
            xticklabels={,,},
            yticklabels={,,},
            xmin = -0.1,
            xmax = 1.1,
            ymin = -0.1,
            ymax = 1.1,
            height = 9cm,
            width = 9cm
        ]
    
        \path (axis cs: 0, 0) coordinate (origin);
        \path (axis cs: 1, 0) coordinate (xAxisAt1);
        \draw[gray!30] let \p1=(origin),\p2=(xAxisAt1), \n1={veclen(\x2-\x1,\y2-\y1)} in (origin) circle[radius=\n1];      
    
        \coordinate (point1) at (axis cs: 0.5, 0.866);
        \coordinate (point1Base) at (axis cs: 0.5, 0);
        \coordinate (point2) at (axis cs: 0.95, 0.312);
        \coordinate (point2Base) at (axis cs: 0.95, 0);
        \coordinate (point3) at (axis cs: 0.7, 0.2299);
        \coordinate (point4) at (axis cs: 0.5, 0.2299);
        \coordinate (point5) at (axis cs: 0.7, 0.866);
        \coordinate (point5Base) at (axis cs: 0.7, 0);
        
        \draw[] (axis cs: 0.7, 0) -- (point3) -- (point1);
    
        \pic[draw=darkblue, angle radius=1cm] {angle=point2--origin--point1};
        \node[darkblue] at (axis cs: 0.18, 0.15) {\scriptsize $\beta$};
        \pic[draw=darkgreen, angle radius=1cm] {angle=point1Base--origin--point2};
        \node[darkgreen] at (axis cs: 0.22, 0.03) {\scriptsize $\alpha$};

        \draw[darkblue, thick] (origin) -- (point1) -- (point3) -- cycle;
        \path (origin) -- (point3) node[above, midway, darkblue, sloped] {$\cospar{\beta}$};
        \path (point3) -- (point1) node[below, midway, sloped, darkblue] {$\sinpar{\beta}$};

        \draw[] (point1) -- (point5) -- (axis cs:0.7,0.2299) -- cycle;
        \pic[draw=darkgreen, angle radius=1cm] {angle=point5--point3--point1};
        \node[darkgreen] at (axis cs: 0.67, 0.45) {\scriptsize $\alpha$};

        \draw[darkgreen, thick] (origin) -- (point2) -- (point2Base) -- cycle;
        \path (point2Base)--(point2) node[darkgreen, midway, below, sloped] {$\sinpar{\alpha}$};
        \path (origin)--(point2Base) node[darkgreen, midway, below, sloped] {$\cospar{\alpha}$};

        \draw[thick, lightorange] (axis cs:0,0.866) -- (point1);
        \path (axis cs:0,0.866) -- (point1) node[above,midway,lightorange,sloped] {$\cospar{\alpha+\beta}$};

        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label=right:{$D$}] at (point2){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label={$C$}] at (point1){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label={$B$}] at (point5){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label={[anchor=center,label distance=3mm] 330:$A$}] at (point3){};
        \node[fill,circle,inner sep=0pt,minimum width=1.5mm,label=below:{$E$}] at (point5Base){};


        \end{axis}
    \end{tikzpicture}
\end{center}

Die \textcolor{lightorange}{orange} eingefärbte Strecke entspricht der X-Koordinate von Punkt $C$ und somit dem gesuchten Funktionswert $\cospar{\alpha+\beta}$.
Die Distanz $\overline{OE}$ bildet die Ankathete des Dreiecks $\triangle OEA$. Die Hypotenuse dieses Dreiecks entspricht $\cospar{\beta}$.
Daraus folgt $\cospar{\alpha}=\frac{\textrm{Ank.}}{\textrm{Hyp.}}=\frac{\overline{OE}}{\cospar{\beta}} \implies \overline{OE} = \cospar{\alpha}\cospar{\beta}$.\\

Die Distanz $\overline{CB}$ bildet die Gegenkathete des Dreiecks $\triangle ABC$. Die Hypotenuse dieses Dreiecks entspricht $\sinpar{\beta}$.
Daraus folgt $\sinpar{\alpha}=\frac{\textrm{Geg.}}{\textrm{Hyp.}}=\frac{\overline{CB}}{\sinpar{\beta}} \implies \overline{CB} = \sinpar{\alpha}\sinpar{\beta}$.\\

Nun lässt sich erkennen: $\cospar{\alpha+\beta} = \overline{OE}-\overline{CB} = \cospar{\alpha}\cospar{\beta} - \sinpar{\alpha}\sinpar{\beta}$.
Analog zur obigen Situation kann der Winkel $\beta$ \glqq negativ\grqq{} gedacht werden, um den Fall der Subtraktion der Winkel $\alpha$ und $\beta$ abzudecken.