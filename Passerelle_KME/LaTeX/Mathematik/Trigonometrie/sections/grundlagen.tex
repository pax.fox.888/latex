\section{Definition der trigonometrischen Funktionen am rechtwinkligen Dreieck}
\subsection{Grundlagen}
\paragraph*{Winkelsummensatz} Die Summe der Innenwinkel eines Dreiecks beträgt \textbf{$180\degree$}.
\begin{equation*}
    \alpha + \beta + \gamma = 180\degree
\end{equation*}

\paragraph*{Satz des Pythagoras} Im rechtwinkligen Dreieck ist die Summe der Kathetenquadrate  gleich dem Hypotenusenquadrat.
\begin{align*}
   &c^2 = a^2 + b^2 \\
    &c = \sqrt{a^2 + b^2}
\end{align*}

\paragraph{Trigonometrischer Pythagoras} Der trigonometrische Pythagoras besagt, dass die Quadrate von Sinus und Cosinus addiert $1$ ergeben. 
\begin{equation*}
    (\sinpar{x})^2 + (\cospar{x})^2 = 1
\end{equation*}

\subsection{Sinus, Cosinus, Tangens und Cotangens}
\begin{center}
    \begin{tikzpicture}
        \coordinate(cornerA) at (5,3);
        \coordinate(cornerB) at (0,0);
        \coordinate(cornerC) at (5,0);
    
        \draw[thick] (cornerA) -- (cornerB) -- (cornerC) -- cycle;
    
        \path (cornerA) -- (cornerB) node[midway, above] {$c$} node[midway, below, sloped] {{\small Hypotenuse}};
        \path (cornerB) -- (cornerC) node[midway, below] {$a$} node[midway, above] {{\small Kathete}};
        \path (cornerC) -- (cornerA) node[midway, right] {$b$} node[midway, left] {\rotatebox{90}{{\small Kathete}}}; 
    
        \node[above right = 0cm and 0cm of cornerA, darkblue] {$A$};
        \node[left = 0.1cm of cornerB, darkblue] {$B$};
        \node[below right = 0cm of cornerC, darkblue] {$C$};
    
        \pic[draw=darkblue, angle eccentricity=1.5, angle radius=0.8cm] {angle=cornerB--cornerA--cornerC};
        \node[below left = 0.2cm and 0cm of cornerA, darkblue] {$\alpha$};
    
        \pic[draw=darkblue, angle eccentricity=1.5, angle radius=1cm] {angle=cornerC--cornerB--cornerA};
        \node[above right = -0.1cm and 0.4cm of cornerB, darkblue] {$\beta$};
    
        \pic[draw=darkblue, angle eccentricity=1.5, angle radius=0.8cm] {angle=cornerA--cornerC--cornerB};
        \node[above left = 0cm and 0.1cm of cornerC, darkblue] {$\gamma$};
    \end{tikzpicture}
\end{center}
Die Hypotenuse ist immer die längste der drei Dreiecksseiten. Für den Winkel $\alpha$ ist die Seite $b$ die \textit{Ankathete} und die Seite $a$ die \textit{Gegenkathete}. Für den Winkel $\beta$ ist die Seite $a$ die \textit{Ankathete} und die Seite $b$ die \textit{Gegenkathete}.

\subsubsection*{Sinus}
Verlängert man die beiden Schenkel des Winkels $\alpha$ (Seiten $c$ und $b$) entsteht ein neues Dreieck, das die selben Winkel wie das Ursprungsdreieck aufweist. \\
Daraus lässt sich ableiten, dass das Verhältnis zwischen Gegenkathete und Hypotenuse für einen Winkel eindeutig ist.\\
\begin{equation*}
    \sinpar{\theta} \coloneqq \frac{\text{Gegenkathete}_{\theta}}{\text{Hypotenuse}} \qquad \text{wobei } \theta = \alpha \text{ oder } \theta = \beta
\end{equation*}

\subsubsection*{Cosinus}
Gleiches lässt sich für die Ankathete ableiten.
\begin{equation*}
    \cospar{\theta} \coloneqq \frac{\text{Ankathete}_{\theta}}{\text{Hypotenuse}} \qquad \text{wobei } \theta = \alpha \text{ oder } \theta = \beta
\end{equation*}

\subsubsection*{Interoperabilität von Sinus und Cosinus}
Da die Gegenkathete von $\alpha$ und die Ankathete von $\beta$ der Dreiecksseite $a$ entspricht und die Ankathete von $\alpha$ und die Gegenkathete von $\beta$ der Dreiecksseite $b$ entspricht, gilt im rechtwinkligen Dreieck:
\begin{gather*}
        \sinpar{\alpha} = \cospar{\beta} \qquad bzw. \qquad \sinpar{\beta} = \cospar{\alpha} \\
        \sinpar{\alpha} = \frac{\text{Gegenkathete}_{\alpha}}{\text{Hypotenuse}} = \frac{a}{c} = \frac{\text{Ankathete}_{\beta}}{\text{Hypotenuse}} = \cospar{\beta} \\
        \cospar{\alpha} = \frac{\text{Ankathete}_{\alpha}}{\text{Hypotenuse}} = \frac{b}{c} = \frac{\text{Gegenkathete}_{\beta}}{\text{Hypotenuse}} = \sinpar{\beta}
\end{gather*}

Auch gelten zwischen Sinus und Cosinus am spitzen Winkel folgende Zusammenhänge:
\begin{gather*}
    \sinpar{\theta} = \cospar{90\degree - \theta}\\
    \cospar{\theta} = \sinpar{90\degree - \theta}
\end{gather*}

\begin{wrapfigure} {r} {2.5cm}
    \begin{tikzpicture}
        \coordinate(origin) at (0,0);
        \coordinate(xMax) at (2,0);
        \coordinate(yMax) at (0,1.5);
        \coordinate(lineEnd) at (2,1.5); 
        
        \draw[thick] (origin) -- (xMax);
        \draw[thick, dashed] (yMax) -- (2, 1.5);

        \draw[thick, darkgreen] (2, 0) -- (lineEnd);
        \draw[thick, darkgreen, dashed] (origin) -- (yMax);

        \draw[thick, darkblue] (origin) -- (lineEnd);

        \pic[draw=darkblue, angle eccentricity=1.5, angle radius=0.6cm]{angle=xMax--origin--lineEnd};
        \node[darkblue] at (0.7,0.25) {{\small $\theta$}};        
        \pic[draw=darkgreen, angle eccentricity = 1.5, angle radius = 0.6cm]{angle=lineEnd--origin--yMax};
        \node[darkgreen, rotate = 34] at (0.6, 1) {{\small $90\degree-\theta$}};

        \pic[draw=darkblue, angle eccentricity=1.5, angle radius=0.6cm]{angle=yMax--lineEnd--origin};
        \pic[draw=darkgreen, angle eccentricity = 1.5, angle radius = 0.6cm]{angle=origin--lineEnd--xMax};

        \path (origin) -- (xMax) node[midway, below] {$a$};
        \path (yMax) -- (lineEnd) node[midway, above] {$a'$};
        \path (xMax) -- (lineEnd) node[midway, right, darkgreen] {$b$};
        \path (origin) -- (yMax) node[midway, left, darkgreen] {$b'$};
    \end{tikzpicture}
\end{wrapfigure}
Da wir uns im rechtwinkligen Dreieck befinden, wissen wir, dass die Summe der spitzen Winkel $90\degree$ beträgt. Durch eine Punktspiegelung des Dreiecks an der Mitte der Hypotenuse entsteht die Abbildung rechts.\\
An ihr wird ersichtlich, dass die ursprüngliche Gegenkathete von $\theta$ nun die Ankathete des Komplementärwinkels $90\degree - \theta$ ist.

\newpage
\clearpage

\subsubsection*{Tangens}
Bei der Veränderung der Längen von Ankatheten und Gegenkatheten bleibt auch deren Verhältnis erhalten. Diese drückt man mit dem Tangens aus.
\begin{align*}
    \tanpar{\theta} &\coloneqq \frac{\text{Gegenkathete}_{\theta}}{\text{Ankathete}_{\theta}} \qquad \text{wobei } \theta = \alpha \text{ oder } \theta = \beta\\
    \tanpar{\theta} &= \frac{\sinpar{\theta}}{\cospar{\theta}}
\end{align*}

\subsubsection*{Cotangens}
Der Cotangens ist der Kehrwert des Tangens.
\begin{align*}
    \cotpar{\theta} &\coloneqq \frac{\text{Ankathete}_{\theta}}{\text{Gegenkathete}_{\theta}} \qquad \text{wobei } \theta = \alpha \text{ oder } \theta = \beta\\
    \cotpar{\theta} &= \frac{1}{\tanpar{\theta}} = \frac{\cospar{\theta}}{\sinpar{\theta}}
\end{align*}

\subsubsection*{Interoperabilität von Tangens und Cotangens}
Wie bei Sinus und Cosinus besteht auch zwischen Tangens und Cotangens.
\begin{gather*}
    \tanpar{\theta} = \frac{1}{\cotpar{\theta}} \qquad \text{und} \qquad \cotpar{\theta} = \frac{1}{\tanpar{\theta}}\\
    \tanpar{\theta} = \frac{1}{\frac{\text{Ankathete}_{\theta}}{\text{Gegenkathete}_{\theta}}} = 1 \cdot \frac{\text{Gegenkathete}_{\theta}}{\text{Ankathete}_{\theta}} = \frac{\text{Gegenkathete}_{\theta}}{\text{Ankathete}_{\theta}} = \cotpar{\theta}\\
    \cotpar{\theta} = \frac{1}{\frac{\text{Gegenkathete}_{\theta}}{\text{Ankathete}_{\theta}}} = 1 \cdot \frac{\text{Ankathete}_{\theta}}{\text{Gegenkathete}_{\theta}} = \frac{\text{Ankathete}_{\theta}}{\text{Gegenkathete}_{\theta}} = \tanpar{\theta}
\end{gather*}

Auch gelten zwischen Tangens und Cotangens am spitzen Winkel folgende Zusammenhänge:
\begin{gather*}
    \tanpar{\theta} = \cotpar{90\degree - \theta}\\
    \cotpar{\theta} = \tanpar{90\degree - \theta}
\end{gather*}
\begin{wrapfigure} {r} {3cm}
    \begin{tikzpicture}
        \coordinate(origin) at (0,0);
        \coordinate(xMax) at (3,0);
        \coordinate(yMax) at (0,2.25);
        \coordinate(lineEnd) at (3,2.25); 
        
        \draw[thick] (origin) -- (xMax);
        \draw[thick, dashed] (yMax) -- (lineEnd);

        \draw[thick, darkgreen] (xMax) -- (lineEnd);
        \draw[thick, darkgreen, dashed] (origin) -- (yMax);

        \draw[thick, darkblue] (origin) -- (lineEnd);

        \pic[draw=darkblue, angle eccentricity=1.5, angle radius=1cm]{angle=xMax--origin--lineEnd};
        \node[darkblue] at (1.2,0.4) {{\small $\theta$}};
        \pic[draw=darkgreen, angle eccentricity = 1.5, angle radius = 1cm]{angle=lineEnd--origin--yMax};
        \node[darkgreen] at (0.6, 1.2) {{\small $90\degree-\theta$}};

        \path (origin) -- (xMax) node[midway, below] {{\small Ankathete}};
        \path (yMax) -- (lineEnd) node[midway, above] {{\small Gegenkathete}};
        \path (xMax) -- (lineEnd) node[midway, right, darkgreen] {\rotatebox{90}{{\small Gegenkathete}}};
        \path (origin) -- (yMax) node[midway, left, darkgreen] {\rotatebox{90}{{\small Ankathete}}};
    \end{tikzpicture}
\end{wrapfigure}
Auch hier erkennt man, durch Punktspiegelung, dass aus der ursprünglichen Ankathete eine Gegenkathete und aus der ursprünglichen Gegenkathete eine Ankathete wird.