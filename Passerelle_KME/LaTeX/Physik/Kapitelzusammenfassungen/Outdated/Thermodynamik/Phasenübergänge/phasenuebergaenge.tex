% Phasenuebergaenge
%
% sources: 
% Notes by Marco Calisto
% "Physik für Mittelschulen"
%

%%% PACKAGES %%%
% standart
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}


% formatting
\usepackage{geometry}           
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage[hidelinks]{hyperref}
\usepackage{wrapfig}
\usepackage{lmodern}

% symbols etc.
\usepackage{amsmath}           
\usepackage{amssymb}    % \cdots
\usepackage{cancel}     % strike-through terms

% graphics and plots
\usepackage{tikz}         
\usepackage{pgfplots}

% Plot styles
\pgfplotsset{orangemarker/.style={color=orange,only marks,mark=*, mark options={scale=0.5}}}

%%% PREAMBLE %%%
% formatting and title
\geometry{bottom=30mm}
\setlength\parindent{0pt}
\title{\vspace{-3cm}Thermodynamisches Gleichgewicht und Phasenübergänge: Kurztheorie und Beispielaufgaben}
\author{Stefan Gloor}

\pgfplotsset{compat=1.7}                     
\numberwithin{equation}{subsubsection}  % numbering of equations
\pgfdeclarelayer{bg}    % declare background layer
\pgfsetlayers{bg,main}  % set the order of the layers (main is the standard layer)
   
\usetikzlibrary{    shapes,
                    arrows,            % Pfeilform
                    arrows.meta, 
                    chains, 
                    external,          % Externalizing graphics
                    positioning        % Node distance
                }  

\fancypagestyle{TitlePageStyle}{ %% frontpage
    \fancyhead{}
    \cfoot{\today}
    \lfoot{Stefan Gloor}
    \rfoot{Seite \thepage \protect{ }von \pageref{LastPage}}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0.5pt}
}

\cfoot{\today} %% following pages
\lfoot{Stefan Gloor}
\rfoot{Seite \thepage \protect{ }von \pageref{LastPage}}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\pagestyle{fancy}

%%% DOCUMENT %%%
\begin{document}
\maketitle


\thispagestyle{TitlePageStyle}

\newpage

\section{Wärmemenge und innere Energie}
    Bringt man zwei unterschiedlich warme Körper in Kontakt,
    findet zwischen ihnen ein Wärmeaustausch statt. Durch diesen Wärmefluss $\Delta Q$
    stellt sich nach einiger Zeit ein thermisches Gleichgewicht ein. Der kältere Körper hat sich aufgewärmt,
    der warme abgekühlt. Beide Körper besitzen im Gleichgewicht diesselbe Temperatur.\\

    \begin{center}
        \begin{tikzpicture}[scale=0.75]
            \begin{pgfonlayer}{bg}
              
                \fill[blue!50] (0,1) rectangle (2,3);
                \fill[red!50] (2,0) rectangle (4,4);
    
                \shade[left color=blue!50,right color=violet!50] (7,1) rectangle (9,3);
                \shade[left color=violet!50,right color=red!50] (9,0) rectangle (11,4);
    
                \fill[violet!50] (14,1) rectangle (16,3);
                \fill[violet!50] (16,0) rectangle (18,4);

                \node[](u) at (1,-1.2) {$U$: Innere Energie [J]};
                \node[](q) at (1,-2) {$Q$: Wärmemenge [J]};

            \end{pgfonlayer}
            \begin{pgfonlayer}{main}
                \node[](u1) at (1,2) {$U_1$};
                \node[](u2) at (3,2) {$U_2$};
                \node[](u1) at (2,5) {$T_1 < T_2$};
                \node[](arrow1) at (5.5,2) {$\Longrightarrow$};
                \node[](Qarrow) at (9,2) {$\xleftarrow{\Delta Q}$};
                \node[](arrow2) at (12.5,2) {$\Longrightarrow$};
                \node[](u1) at (16,5) {$T_1 = T_2$};
                \node[](u1b) at (15,2) {\small{$U_1+\Delta Q$}};
                \node[](u2b) at (17,2) {\small{$U_2-\Delta Q$}};

            \end{pgfonlayer}

        \end{tikzpicture}
    \end{center}

    Die Temperatur eines Körpers ist direkt von seiner \textbf{inneren Energie} abhängig.
    Die innere Energie bestimmt also, je nach Masse und Material des Körpers, dessen Temperatur.
    In einem isolierten System, bei welchem keine Energie (also keine Wärme) entweichen kann,
    entspricht die Energieabnahme $-\Delta Q$  des warmen Körpers exakt der Energiezunahme $+\Delta Q$
    des kalten Körpers.\\
    Es gilt folgender Zusammenhang:
    \begin{equation}\label{eq:therm}
        \Delta Q = m \cdot c \cdot \Delta \vartheta
    \end{equation}

    $c$ beschreibt die spezifische Wärmekapazität, eine Materialkonstante.\\

    Bei einem idealen Gas entspricht die innere Energie der Summe der kinetischen Energie aller Teilchen.\\


    In einem Gas lässt sich zusätzlich die \textbf{molare spezifische Wärmekapazität} $C_x$ definieren.
    Mit ihr lässt sich beschreiben, wieviel Energie $Q$ einer bestimmten Stoffmenge $n$ zugeführt werden muss, sodass sie sich um
    einen bestimmten Temperaturbetrag $\Delta \vartheta$ erhöht.

    \begin{equation}\label{eq:molar}
        \Delta Q = n \cdot C_x \cdot \Delta\vartheta
    \end{equation}

    Setzten wir (\ref{eq:therm}) und (\ref{eq:molar}) gleich, so erhält man:
    \begin{equation}
        m \cdot c \cdot \cancel{\Delta \vartheta} = n \cdot C_x \cdot \cancel{\Delta\vartheta}
    \end{equation}
    Mithilfe der molaren Masse $M=m/n$ ergibt sich:
    \begin{equation}
        C_x = c \cdot M
    \end{equation}

\section{Phasenübergänge}
    \begin{center}
        \begin{tikzpicture}
            \begin{axis}[   samples=100,
                            inner axis line style={thick},
                            axis lines=center,
                            axis line style={-Latex},
                            xticklabels={,,},
                            yticklabels={,,},
                            xlabel={Zugeführte Wärme $\Delta Q$ [J]},
                            ylabel={Temperatur des Körpers $T$ [K]},
                            x label style={at={(axis description cs:0.5,-0.05)},anchor=north},
                            y label style={at={(axis description cs:-0.05,.5)},rotate=90,anchor=south},
                            ymax=11,
                            xmax=15,
                            width=12cm,
                            height=7cm,
                            ]
            \addplot[domain=0:3,black,thick] {x};
            \addplot[domain=3:5,blue!50!black,thick] {3};
            \addplot[domain=5:10,blue,thick] {x-2};
            \addplot[domain=10:12,violet,thick] {8};
            \addplot[domain=12:13,red,thick] {x-4};

            \addplot[mark=none, black, dotted] coordinates {(3,0) (3,12)};
            \addplot[mark=none, black, dotted] coordinates {(5,0) (5,12)};
            \addplot[mark=none, black, dotted] coordinates {(10,0) (10,12)};
            \addplot[mark=none, black, dotted] coordinates {(12,0) (12,12)};

            \node at (axis cs:1.5, 10){Fest};
            \node at (axis cs:7.5, 10){Flüssig};
            \node at (axis cs:13.5, 10){Gasförmig};

            \node at (axis cs:4, 2.5){{$\underbrace{\phantom{XXXX}}_{Q_f}$}};
            \node at (axis cs:11, 7.5){{$\underbrace{\phantom{XXXX}}_{Q_v}$}};

            \end{axis}
        \end{tikzpicture}
    \end{center}

    Man stelle sich einen Klotz Eis vor, dem kontinuierlich Wärmeenergie zugeführt wird.
    Zuerst steigt die Temperatur des Eises linear mit der zugeführten Wärme $\Delta Q$ an,
    bis das Eis zu schmelzen beginnt.\\

    In der Übergangsphase Eis-Wasser bleibt die Temperatur \textbf{konstant bei 0 $^{\circ}$C}, auch
    wenn immer noch kontinuierlich Wärme zugeführt wird. Das geschieht solange, bis alles Eis geschmolzen
    ist. Ab diesem Punkt beginnt die Temperatur wieder linear zu steigen.\\

    An der nächsten Übergangsphase Wasser-Dampf bleibt die Temperatur erneut \textbf{konstant bei 100 $^{\circ}$C}, 
    bis das komplette Wasser verdampft ist. Erst dann nimmt die Temperatur des Dampfes wieder zu.

    Die Wärmemenge $Q_f$, die nötig ist, um Eis von 0 $^{\circ}$C in Wasser derselben Temperatur zu überführen, 
    nennt man \textbf{Schmelzwärme}. Analog dazu nennt man $Q_v$ \textbf{Verdampfungswärme}.
    Sie berechnet sich aus der spezifischen Schmelz- bzw. Verdampfungswärme $L_f$ resp. $L_v$.
    \begin{equation}
        Q_f=L_f \cdot m
    \end{equation}

    \paragraph{Beispiel.} Wieviel Wasserdampf ($m_{\textrm{Dmf}}$) von 100 $^{\circ}$C muss man in 
    $m_{\textrm{W}}=100$ g Wasser, welches noch $m_{\textrm{Eis}}=100$ g Eis enthält, geben, damit am Ende 
    eine Temperatur von $\vartheta_{\textrm{M}}=$18 $^{\circ}$C entsteht?\\

    Der Wärmetransport findet vom heissen Dampf in das kalte Eiswasser statt. 
    Da der Dampf vollständig kondensiert, wird Verdampfungswärme frei.
    Ebenfalls muss man Schmelzwärme des Eises berücksichtigen, weil auch das Eis komplett schmilzt.
    Wir nehmen an, dass es sich um ein isoliertes System handelt und beginnen mit der Gleichung:

    \begin{align*} 
        \Delta Q_{ab} &= \Delta Q_{zu} \\ 
        \underbrace{m_{\textrm{Dmf}} \cdot c_{\textrm{W}} \cdot (\vartheta_{\textrm{Dmf}} - \vartheta_{\textrm{M}})}_{\substack{\textrm{Wärmemenge des kon-}\\ \textrm{densierten Dampfes,}\\ \textrm{bei Abkühlung bis $\vartheta_M$}}} + \underbrace{m_{\text{Dmf}} \cdot L_v}_{\substack{\textrm{Verdampfungs-}\\ \textrm{wärme}}} &= \underbrace{(m_{\textrm{Eis}}\cdot c_{\textrm{Eis}}+m_{\textrm{W}} \cdot c_{\textrm{W}}) \cdot (\vartheta_{\textrm{M}}-\vartheta_{\textrm{EisW}})}_{\substack{\textrm{Wärmemenge, um Wasser und} \\ \textrm{geschmolzenes Eis auf $\vartheta_M$ aufzuwärmen}}} + \underbrace{m_{\textrm{Eis}}\cdot L_f}_{\textrm{Schmelzwärme}}\\
        m_{\textrm{Dmf}} \cdot (c_W\cdot\vartheta_M - c_W \cdot \vartheta_{\textrm{M}} + L_v) &= (m_{\textrm{Eis}}\cdot c_{\textrm{Eis}}+m_{\textrm{W}} \cdot c_{\textrm{W}}) \cdot (\vartheta_{\textrm{M}}-\vartheta_{\textrm{EisW}}) + m_{\textrm{Eis}}\cdot L_f\\
        m_{\textrm{Dmf}} &= \frac{(m_{\textrm{Eis}}\cdot c_{\textrm{Eis}}+m_{\textrm{W}} \cdot c_{\textrm{W}}) \cdot (\vartheta_{\textrm{M}}-\vartheta_{\textrm{EisW}}) + m_{\textrm{Eis}}\cdot L_f}{(c_W\cdot\vartheta_M - c_W \cdot \vartheta_{\textrm{M}} + L_v)}\\
    \end{align*}
    Nun setzen wir alle uns bekannten Werte ein und erhalten das gesuchte Resultat:
    \begin{align*}
        m_{\textrm{Dmf}} &= \frac{ 0.1\textrm{ kg} \cdot 2100 \textrm{ J/(kg$\cdot$K)} +0.1\textrm{ kg} \cdot 4182 \textrm{ J/(kg$\cdot$K)} \cdot (18^{\circ} \textrm{C}-0^{\circ} \textrm{C}) + 0.1\textrm{ kg}\cdot 3.338 \cdot 10^{5} \textrm{ J/kg}}{4182\textrm{ J/(kg$\cdot$K)}\cdot 18^{\circ} \textrm{C} - 4182\textrm{ J/(kg$\cdot$K)} \cdot 18^{\circ} \textrm{C} + 22.56 \cdot 10^{5}\textrm{ J/kg})}\\
        \underline{\underline{ m_{\textrm{Dmf}}}} &= \underline{\underline{1.823 \cdot 10^{-2} \textrm{ kg}}}
    \end{align*}

    \paragraph{Beispiel II. }
        1 kg Wasser sind auf $-9$ $^{\circ}$C überführt worden; durch Hinzufügen eines Kristallisationskeimes wird das Gefrieren 
        eingeleitet. Man berechne die Wassermenge, die erstarrt.\\

        Durch die Bildung der Eiskristalle wird Schmelzwärme frei. Diese Wärme wärmt das Eiswasser auf $0$ $^{\circ}$C auf.
        \begin{align*} 
            \underbrace{m_{\textrm{Eis}} \cdot L_f}_{\substack{\textrm{Schmelzwärme, die durch}\\\textrm{das Erstarren frei wird}}} &= \underbrace{m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \Delta \vartheta}_{\substack{\textrm{Nötige Wärmeenergie, um}\\\textrm{Wasser auf $0^{\circ}$ \textrm{C} aufzuwärmen.}}}\\ 
            m_{\textrm{Eis}} &= \frac{m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \Delta \vartheta}{L_f}\\
            m_{\textrm{Eis}} &= \frac{1\textrm{ kg} \cdot 4182 \textrm{ J/(kg$\cdot$K)} \cdot 9^{\circ} \textrm{C} }{3.338 \cdot 10^{5} \textrm{ J/kg}}\\
            \underline{\underline{ m_{\textrm{Eis}}}} &= \underline{\underline{1.128 \cdot 10^{-1} \textrm{ kg}}}
        \end{align*}

\section{Aufgaben}
        \subsection{Aufgabe 18, Aufgabensammlung Wärme}
            Ihr Badewasser von 45 $^{\circ}$C (und 329 Liter, s. Aufgabe 14) ist Ihnen nun doch
            etwas zu heiss. Sie werfen deshalb 2 kg Eis von 0 $^{\circ}$C hinein. Welche neue Mischtemperatur
            erwarten Sie?\\

            Das Badewasser wird auf die Mischtemperatur abgekühlt. Es wird also Wärmeenergie an das Eis abgegeben,
            um dieses zuerst vollständig zu schmelzen (Schmelzwärme) und anschliessend ebenfalls auf die Mischtemperatur
            zu erwärmen.

            \begin{align*} 
                 \underbrace{m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \Delta \vartheta_W}_{\textrm{Abkühlen des Wassers}} &= &\underbrace{m_{\textrm{Eis}} \cdot L_f}_{\textrm{Schmelzwärme}} &+ &\underbrace{m_{\textrm{Eis}} \cdot c_{\textrm{W}} \cdot \Delta \vartheta_{\textrm{Eis}}}_{\textrm{Aufwärmen des geschmolzenen Eises}}\\
                 m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot (\vartheta_\textrm{W} - \vartheta_\textrm{M}) &= &m_{\textrm{Eis}} \cdot L_f &+ &m_{\textrm{Eis}} \cdot c_{\textrm{W}} \cdot (\vartheta_{\textrm{M}} - \vartheta_{\textrm{Eis}})\\
                 m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \vartheta_\textrm{W} - m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \vartheta_\textrm{M} &= &m_{\textrm{Eis}} \cdot L_f &+ &m_{\textrm{Eis}} \cdot c_{\textrm{W}} \cdot \vartheta_{\textrm{M}} - \cancel{m_{\textrm{Eis}} \cdot c_{\textrm{W}} \cdot 0^{\circ} \textrm{C}}\\
                 -m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \vartheta_\textrm{M} - m_{\textrm{Eis}} \cdot c_{\textrm{W}} \cdot \vartheta_\textrm{M} &= &m_{\textrm{Eis}} \cdot L_f &- &m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \vartheta_{\textrm{W}}\\
                 -\vartheta_{\textrm{M}} \cdot (m_{\textrm{W}} \cdot c_{\textrm{W}} + m_{\textrm{Eis}} \cdot c_{\textrm{W}}) &= &m_{\textrm{Eis}} \cdot L_f &- &m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \vartheta_{\textrm{W}}\\
                 -\vartheta_{\textrm{M}} \cdot c_{\textrm{W}} \cdot (m_{\textrm{W}} + m_{\textrm{Eis}}) &= &m_{\textrm{Eis}} \cdot L_f &- &m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \vartheta_{\textrm{W}}
            \end{align*}
                Nach $\vartheta_{\textrm{M}}$ auflösen und einsetzen:
            \begin{align*}   
                 \vartheta_{\textrm{M}} &= \frac{m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \vartheta_{\textrm{W}} - m_{\textrm{Eis}} \cdot L_f}{c_{\textrm{W}} \cdot (m_{\textrm{W}} + m_{\textrm{Eis}}) }\\
                 \vartheta_{\textrm{M}} &= \frac{329\textrm{ kg} \cdot 4182 \textrm{ J/(kg$\cdot$K)} \cdot 45^{\circ} \textrm{C}- 2\textrm{ kg} \cdot 3.338 \cdot 10^{5} \textrm{ J/kg} }{4182 \textrm{ J/(kg$\cdot$K)}  \cdot (329\textrm{ kg} + 2\textrm{ kg}) }\\
                 \underline{\underline{\vartheta_{\textrm{M}}}} &= \underline{\underline{44.25^{\circ} \textrm{C}}}
            \end{align*}

        \subsection{Aufgabe 19, Aufgabensammlung Wärme}
            Zur Kühlung des KKW Gösgen wird Flusswasser der Temperatur 20 $^{\circ}$C in einem Kühlturm verdampft. Wie viele Liter Wasser werden pro Stunde verdampft, wenn eine
            thermische Leistung von 2.8 GW abgeführt werden muss?\\

            Wir teilen die Aufgabe in zwei Teile auf: Zuerst soll bestimmt werden, wie viel Wärmeenergie vom AKW in einer Stunde produziert wird.
            Danach können wir mit dieser Angabe bestimmen, wieviel Flusswasser zum Abtransport eben dieser Wärmemenge nötig ist.\\

            \begin{align*}
                Q&=P \cdot t\\
                Q&=2.8 \cdot 10^9 \textrm{W} \cdot 3600 \textrm{s}\\
                \underline{Q}&=\underline{1.008 \cdot 10^{13} \textrm{J}}
            \end{align*}

            Diese Wärmemenge $Q$ entspricht derjenigen, die in einer Stunde vom AKW produziert wird. Nun möchten 
            wir diese Wärmemenge mit dem Verdampfen von Flusswasser abführen. Das Wasser wird zu erst von 20 $^{\circ}$C auf 100 $^{\circ}$C
            erwärmt, danach vollständig verdampft.

            \begin{align*}
                Q &= \underbrace{L_v \cdot m_{\textrm{W}}}_{\textrm{Verdampfungswärme}} + \underbrace{m_{\textrm{W}} \cdot c_{\textrm{W}} \cdot \Delta \vartheta}_{\textrm{Erwärmung von 20 $^{\circ}$C auf 100 $^{\circ}$C}}\\
                Q &= m_{\textrm{W}} \cdot (L_v + c_{\textrm{W}} \cdot \Delta \vartheta)\\
                m_{\textrm{W}} &= \frac{Q}{(L_v + c_{\textrm{W}} \cdot \Delta \vartheta)}\\
                m_{\textrm{W}} &= \frac{1.008 \cdot 10^{13} \textrm{ J}}{(22.56 \cdot 10^{5}\textrm{ J/kg} + 4182 \textrm{ J/(kg$\cdot$K)} \cdot 80^{\circ} \textrm{C})}\\
                \underline{\underline{m_{\textrm{W}} }} &= \underline{\underline{3.891 \cdot 10^{6} \textrm{ kg}}}
            \end{align*}
            
    \footnote{Die Aufgaben stammen aus https://www.sqrt.ch/Physik/Waerme/Aufg-Waerme.pdf})

\end {document}
