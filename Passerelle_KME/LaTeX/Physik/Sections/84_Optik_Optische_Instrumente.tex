\clearpage % flushes out all floats
\subsection{Optische Instrumente}
\subsubsection{Fernrohre (Teleskope)}

Fernrohre werden verwendet, um sehr weit entfernte Objekte zu vergrössern. In den meisten Fällen kann davon ausgegangen werden, dass das betrachtete Objekt unendlich weit entfernt ist. \\\hspace*{\fill}

Es gibt zwei Typen astronomischer Fernrohre. Das gewöhnliche Kepler-Fernrohr besteht aus zwei Sammellinsen, die an den gegenüberliegenden Enden eines langen Rohres stehen. Die dem Objekt nächstgelegene Linse wird 
    als \textbf{Objektiv} bezeichnet. Es erzeugt in der Ebene seines Brennpunktes ein reales Bild $P$ eines weit entfernten Objektes (oder in dessen Nähe, falls das Objekt nicht unendlich weit entfernt ist). Obwohl dieses Bild kleiner ist als das ursprüngliche Objekt, überdeckt es einen grösseren Winkel und ist sehr nahe an der zweiten Linse, dem sogenannten \textbf{Okular}, das als Vergrösserungsglas fungiert. Das Okular vergrössert also das durch das Objektiv erzeugte Bild, wodurch ein zweites, stark vergrössertes Bild $U$ entsteht, welches \textbf{virtuell und invertiert} ist. \\\hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Filled area
            \draw[fill=MyRed!50,draw=none,path fading=east] (0,1.2) -- (2.5,0.7) -- (6,-0.7) -- (7.5,-0.7) -- (7.5,-0.388) -- (7.5,-0.926) -- (7.5,-1.3) -- (6,-0.7) -- (2.5,-0.7) -- (0,-0.2) --  cycle;

            % Arrows
            %% Gegenstand & Bild
            \draw[draw=none,fill=MyBlack!75] (0.95,0) -- (0.95,-3.38) -- (0.85,-3.38) -- (1,-3.73) -- (1.15,-3.38) -- (1.05,-3.38) -- (1.05,0) -- cycle; % B (virtuell)
            \draw[draw=none,fill=MyBlack!75] (5.95,0) -- (5.95,-0.35) -- (5.85,-0.35) -- (6,-0.7) -- (6.15,-0.35) -- (6.05,-0.35) -- (6.05,0) -- cycle node[anchor=west, shift={(1.5mm,-3.5mm)}, text=MyBlack] {$h$}; % G (reell)
            
            % Lens
            \draw[thick,draw=MyGreen,fill=MyGreen!60] (2.5,2) arc[start angle=150,delta angle=60,radius=4] arc[start angle=-30,delta angle=60,radius=4] -- cycle; % Objektiv
            \draw[thick,white] (2.5,2.05) -- (2.5,-2.05) node[anchor=center, shift={(0mm,-2mm)}, text=MyBlack] {\small Objektiv}; % Objektiv
            \draw[thick,draw=MyGreen,fill=MyGreen!60] (7.5,2) arc[start angle=150,delta angle=60,x radius=2.5,y radius=4] arc[start angle=-30,delta angle=60,x radius=2.5,y radius=4] -- cycle; % Okular
            \draw[thick,white] (7.5,2.05) -- (7.5,-2.05) node[anchor=center, shift={(0mm,-2mm)}, text=MyBlack] {\small Okular}; % Okular
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (0,0) -- (10,0);

            % Lengths
            %% B
            \draw[thick,MyBlack] (0,-3.73) -- (1,-3.73);
            \draw[<->,thick,MyBlack] (0.5,0) -- (0.5,-3.73) node[pos=0.5, anchor=east, shift={(0mm,0mm)}, text=MyBlack] {$B$};
            %% f_e
            \draw[thick,MyBlack] (6,0.75) -- (6,2.5);
            \draw[thick,MyBlack] (7.5,2) -- (7.5,2.5);
            \draw[thick,MyBlack] (9,0.75) -- (9,2.5);
            \draw[<->,thick,MyBlack] (6,2.25) -- (7.5,2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f_e$};
            \draw[<->,thick,MyBlack] (7.5,2.25) -- (9,2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f_e$};
            %% f_0
            \draw[thick,MyBlack] (2.5,2) -- (2.5,2.5);
            \draw[<->,thick,MyBlack] (2.5,2.25) -- (6,2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f_0$};

            % Angles
            \draw[MyBlack] (0.5,0) arc (200:153.5:0.5);
            \node[color=MyBlack] at (0.3,0.2) {$\phi$}; % text
            \draw[MyBlack] (4,0) arc (20:-15:0.5);
            \node[color=MyBlack] at (4.2,-0.15) {$\phi$}; % text
            \draw[MyBlack] (8.25,0) arc (180:217.5:0.5);
            \node[color=MyBlack] at (8.05,-0.25) {$\phi'$}; % text
            
            % Light rays
            \draw[->,thick,MyRed] (0,0.5) -- (7.5,-1) -- (10,0.05);
            \draw[->,thick,MyRed] (0,-0.2) -- (2.5,-0.7) -- (7.5,-0.7) -- (10,0.47);
            \draw[->,thick,MyRed] (0,1.2) -- (2.5,0.7) -- (7.5,-1.3) -- (10,-0.365);
            
            \draw[thick,MyRed,dashed] (1,-3.73) -- (7.5,-0.7);
            \draw[thick,MyRed,dashed] (1,-3.73) -- (7.5,-1);
            \draw[thick,MyRed,dashed] (1,-3.73) -- (7.5,-1.3);
            
            % Points
            \draw[draw=MyBlack,fill=white] (6,-0.7) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$P$};
            \draw[draw=MyBlack,fill=white] (1,-3.73) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$U$};
            \draw[draw=MyBlack,fill=white] (1,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$V$};
            \draw[draw=MyBlack,fill=white] (2.5,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$X$};

            \draw[draw=MyRed,fill=white] (9,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_1$};
            \draw[draw=MyRed,fill=white] (6,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_2$};

            \node[text=MyBlack, align=left, font=\scriptsize] at (-1.25,0.75) {Parallelstrahlen\\von einem Objekt\\bei unendlicher\\Entfernung}; % text

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Strahlengang in einem Kepler-Fernrohr.}
    \label{fig:Strahlengang in einem Kepler-Fernrohr}
\end{figure}

Um die Gesamtvergrösserung dieses Fernrohres zu bestimmen, bemerken wir, dass der Winkel, den ein von einem blossen Augen betrachtetes Objekt überdeckt, gerade der vom Objektiv überdeckte Winkel $\phi$ ist. Aus der 
    Abbildung \ref{fig:Strahlengang in einem Kepler-Fernrohr} erkennen wir, dass folgender Zusammenhang gilt:
    \begin{equation}
        \phi \approx \frac{h}{f_0} \label{eq:Fernrohr Winkel phi}
    \end{equation}

Hierbei ist $h$ die Höhe des Bildes $P$. Die Gesamtvergrösserung (Winkelvergrösserung) dieses Fernrohres wird wie folgt berechnet:
    \begin{equation}
        v = \frac{\phi'}{\phi} = \frac{-f_0}{f_e} \label{eq:Gesamtvergroesserung Fernrohr}
    \end{equation}

Das Minuszeichen bedeutet, dass das Bild invertiert ist. Um eine starke Vergrösserung zu erzielen, muss das Objekt eine grosse und das Okular eine kleine Brennweite haben.
\newpage

\subsubsection{Mikroskope}

Das Mikroskop besteht wie das Fernrohr aus einem Okular und einem Objektiv. Der Aufbau eines Mikroskops unterscheidet sich von dem des Fernrohres, da es zum Betrachten sehr naher Objekte verwendet wird. Dies 
    bedeutet, dass die die \textbf{Objektweite sehr klein} ist. \\\hspace*{\fill}

Das Objekt wird unmittelbar \textbf{hinter dem Brennpunkt des Objektives} platziert. Das durch das Objektiv erzeugte Bild $P_2$ ist real, weit weg von der Linse und stark vergrössert. Dieses Bild wird durch das 
    Okular zu einem sehr grossen, virtuellen und invertierten Bild $U$ vergrössert, welches vom Auge gesehen wird.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Filled area
            \draw[fill=MyRed!50,draw=none,path fading=east] (1.75,0.5) -- (4,0.5) -- (7.15,-0.7) -- (8.5,-0.7) -- (9.5,-0.18) -- (9.5,-0.31) -- (8.5,-1.2) -- (7.15,-0.7) -- (4,-0.7) --  cycle;

            % Arrows
            %% Gegenstand & Bild
            \draw[draw=none,fill=MyBlack!75] (-0.05,0) -- (-0.05,-4.757) -- (-0.15,-4.757) -- (0,-5.107) -- (0.15,-4.757) -- (0.05,-4.757) -- (0.05,0) -- cycle; % B (virtuell)
            \draw[draw=none,fill=MyBlack!75] (1.7,0) -- (1.7,0.15) -- (1.6,0.15) -- (1.75,0.5) -- (1.9,0.15) -- (1.8,0.15) -- (1.8,0) -- cycle node[anchor=east, shift={(-0.75mm,2.5mm)}, text=MyBlack] {$G_1$}; % G_1 (reell)
            \node[color=MyBlack] at (1.75,-0.25) {\small Objekt}; % text
            \draw[draw=none,fill=MyBlack!75] (7.1,0) -- (7.1,-0.35) -- (7,-0.35) -- (7.15,-0.7) -- (7.3,-0.35) -- (7.2,-0.35) -- (7.2,0) -- cycle node[anchor=west, shift={(1.5mm,-3.5mm)}, text=MyBlack] {$G_2$}; % G_2 (reell)
            
            % Lens
            \draw[thick,draw=MyGreen,fill=MyGreen!60] (4,1) arc[start angle=150,delta angle=60,radius=2] arc[start angle=-30,delta angle=60,radius=2] -- cycle; % Objektiv
            \draw[thick,white] (4,1.05) -- (4,-1.05) node[anchor=center, shift={(0mm,-2mm)}, text=MyBlack] {\small Objektiv}; % Objektiv

            \draw[thick,draw=MyGreen,fill=MyGreen!60] (8.5,2) arc[start angle=150,delta angle=60,radius=4] arc[start angle=-30,delta angle=60,radius=4] -- cycle; % Okular
            \draw[thick,white] (8.5,2.05) -- (8.5,-2.05) node[anchor=center, shift={(0mm,-2mm)}, text=MyBlack] {\small Okular}; % Okular
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (-1,0) -- (11,0);

            % Lengths
            %% B
            \draw[thick,MyBlack] (-1,-5.107) -- (0,-5.107);
            \draw[<->,thick,MyBlack] (-0.5,0) -- (-0.5,-5.107) node[pos=0.5, anchor=east, shift={(0mm,0mm)}, text=MyBlack] {$B$};
            %% f_e
            \draw[thick,MyBlack] (7.15,0.75) -- (7.15,2.5);
            \draw[thick,MyBlack] (8.5,2) -- (8.5,3.25);
            \draw[thick,MyBlack] (9.85,0.75) -- (9.85,2.5);
            \draw[<->,thick,MyBlack] (7.15,2.25) -- (8.5,2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f_e$};
            \draw[<->,thick,MyBlack] (8.5,2.25) -- (9.85,2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f_e$};
            %% l
            \draw[thick,MyBlack] (4,1) -- (4,3.25);
            \draw[<->,thick,MyBlack] (4,3) -- (8.5,3) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$l$};
            %% d_0
            \draw[thick,MyBlack] (1.75,1.25) -- (1.75,3.25);
            \draw[<->,thick,MyBlack] (1.75,3) -- (4,3) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$d_0$};
            
            % Light rays
            \draw[->,thick,MyRed] (1.75,0.5) -- (4,0.5) -- (8.5,-1.2) -- (11,-0.051);
            \draw[->,thick,MyRed] (1.75,0.5) -- (8.5,-1) -- (11,0.2079);
            \draw[->,thick,MyRed] (1.75,0.5) -- (4,-0.7) -- (8.5,-0.7) -- (11,0.596);
            
            \draw[thick,MyRed,dashed] (0,-5.107) -- (8.5,-0.7);
            \draw[thick,MyRed,dashed] (0,-5.107) -- (8.5,-1);
            \draw[thick,MyRed,dashed] (0,-5.107) -- (8.5,-1.2);
            
            % Points
            \draw[draw=MyBlack,fill=white] (1.75,0.5) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$P_1$};
            \draw[draw=MyBlack,fill=white] (7.15,-0.7) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$P_2$};
            \draw[draw=MyBlack,fill=white] (0,-5.107) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$U$};
            \draw[draw=MyBlack,fill=white] (0,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$V$};
            \draw[draw=MyBlack,fill=white] (4,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$X$};

            \draw[draw=MyRed,fill=white] (2.6875,0) circle (0.05) node[anchor=north, shift={(0mm,-1mm)}, text=MyBlack] {$F_1$};
            \draw[draw=MyRed,fill=white] (5.3125,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_1$};
            \draw[draw=MyRed,fill=white] (7.15,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_2$};
            \draw[draw=MyRed,fill=white] (9.85,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_2$};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Strahlengang in einem Mikroskop.}
\end{figure}

Die Gesamtvergrösserung eines Mikroskops ist das Produkt der Vergrösserung der beiden einzelnen Linsen. Das durch das Objektiv erzeugte Bild $P_2$ ist um den Faktor $v_0$ grösser als das Objekt:
    \begin{equation}
        v_0 = \frac{l - f_e}{d_0} \label{eq:Gesamtvergroesserung Mikroskop}
    \end{equation}