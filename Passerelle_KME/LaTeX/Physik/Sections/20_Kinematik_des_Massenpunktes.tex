\clearpage % flushes out all floats
\section{Kinematik des Massenpunktes}
\subsection{Geradlinig gleichförmige Bewegung}

Die Bewegung eines Körpers (Massepunktes) heisst \textbf{geradlinig gleichförmig}, wenn dieser in einer gleichen Zeitabschnitten gleiche Strecken zurücklegt, sich also mit einer konstanten Geschwindigkeit 
    \textit{v}, deren Richtung und Betrag konstant ist, bewegt. \\\hspace*{\fill}

Die geradlinig gleichförmige Bewegung beschreibt den Zustand des Gleichgewichts eines Körpers: Ein Körper ist genau dann im Gleichgewicht, wenn er sich mit konstanter Geschwindigkeit geradlinig bewegt oder wenn er 
    in Ruhe ist. Seine Beschleunigung beträgt Null. \\\hspace*{\fill}

Physikalisch verstehen wir eine geradlinig gleichförmige Bewegung am besten, wenn wir den zurückgelegten Weg \textit{s}, die Geschwindigkeit \textit{v} und die Beschleunigung 
    \textit{a} als Funktion der Zeit \textit{t} in Diagrammen darstellen und untersuchen. \\\hspace*{\fill}

Unter der Geschwindigkeit \textit{v} eines Körpers wird das Verhältnis zwischen dem zurückgelegten Weg $\Delta s$ und der dazu erforderlichen Zeit $\Delta t$ verstanden:
    \begin{equation}
        v = \frac{\Delta s}{\Delta t} = \frac{s_2 - s_1}{t_2 - t_1} \ [\si{\metre/\second}] \label{eq:Geschwindigkeit gerad. gleichf. Beweg.}
    \end{equation}
    


Aus diesem Ausdruck lassen sich die Formel für den Weg \textit{s} und für die Zeit \textit{v} ableiten:
    \begin{gather} 
        s = v \cdot t \ [\si{\metre}] \label{eq:Strecke gerad. gleichf. Beweg.} \\
        t = \frac{s}{v} \ [\si{\second}] \label{eq:Zeit gerad. gleichf. Beweg.}
    \end{gather}

Ein Körper, der sich mit konstanter Geschwindigkeit, d.h. immer gleich schnell in der gleichen Richtung bewegt, führt eine geradlinig gleichförmige Bewegung aus. \\\hspace*{\fill}

Ändert sich die Geschwindigkeit eines bewegten Körpers, so führt er eine \textbf{beschleunigte Bewegung} aus. Zur Beschreibung beschleunigter Bewegungen wird die Beschleunigung \textit{a} eingeführt:
    \begin{equation}
        a = \frac{\Delta v}{\Delta t} = \frac{v_2 - v_1}{t_2 - t_1} \ [\si{\metre/\second}^2] \label{eq:Beschleunigung einer Beweg.}
    \end{equation}

Die Beschleunigung drückt die Geschwindigkeitsänderung pro Zeiteinheit aus.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            %--------------%
            % Left Picture %
            %--------------%
            
            % Axes
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (-1,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (-1,-1) -- (2,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (-1,-1) -- (-1,2) node[anchor=east, shift={(0,-2.5mm)}] {$v \, [\si{\metre/\second}]$}; % y axis
            \draw[thick] (-1,-0.75) -- (-1,-1) -- (-0.75,-1); % make axes look connected

            % Line
            \draw[MyLightBlue,very thick] (-1,1) -- (1.5,1);
            
            %----------------%
            % Middle Picture %
            %----------------%      
    
            % Arc
            \draw[MyLightBlue,very thick] (4.5,-1) arc (0:45:1) node[anchor=north, shift={(-1mm,-2mm)}] {$45\si{\degree}$};

            % Axes
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,-1) -- (6.5,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$s \, [\si{\metre}]$}; % y axis
            \draw[thick] (3.5,-0.75) -- (3.5,-1) -- (3.75,-1); % make axes look connected
            
            % Line
            \draw[MyLightBlue,very thick] (3.5,-1) -- (6,1.5);
                
            %---------------%
            % Right Picture %
            %---------------%      
            
            % Axes
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 9,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (8,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (8,-1) -- (11,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (8,-1) -- (8,2) node[anchor=east, shift={(0,-2.5mm)}] {$a \, [\si{\metre/\second}^2]$}; % y axis
            \draw[thick] (8,-0.75) -- (8,-1) -- (8.25,-1); % make axes look connected
    
            % Line
            \draw[MyLightBlue,very thick] (8,-1) -- (10.5,-1);
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{v-t-Diagramm für eine geradlinig gleichförmige Bewegung.}
\end{figure}
\newpage

\subsubsection{Mittlere Geschwindigkeit}

Bewegt sich ein Körper nicht gleichförmig, dann bezeichnet man den Quotienten aus Ortsänderung \textit{s} und Zeitänderung \textit{v} als die mittlere Geschwindigkeit der nicht gleichförmigen Bewegung. \\\hspace*{\fill}

Um die mittlere Geschwindigkeit eines Körpers zu bestimmen, muss zunächst den Ort des Körpers zu zwei unterschiedlichen Zeitpunkten gemessen werden. Für die Differenz der Orte wird der Ort des späteren Zeitpunkts 
    von dem Ort des früheren Zeitpunkts subtrahiert. Dasselbe wird mit den Zeitpunkten gemacht, um die verstrichene Zeit zu erhalten:
    \begin{equation}
        v = \frac{s_1 + s_2 + \text{\dots} + s_n}{t_1 + t_2 + \text{\dots} + t_n}
        = \frac{t_1 \cdot v_1 + t_2 \cdot v_2 + \text{\dots} + t_n \cdot v_n}{t_1 + t_2 + \text{\dots} + t_n} \label{eq:Mittlere Geschwindigkeit}
    \end{equation}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,-1) -- (6.5,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$v \, [\si{\metre/\second}]$}; % y axis
            \draw[thick] (3.5,-0.75) -- (3.5,-1) -- (3.75,-1); % make axes look connected
            
            % Line
            \draw[MyLightBlue,very thick] (3.5,0) -- (4.5,0) -- (4.5,1.5) -- (5.5,1.5) -- (5.5,0.5) -- (6,0.5);
            \draw[MyLightBlue,very thick,dashed] (3.5,1) -- (6,1) node[anchor=west, shift={(0,0mm)}] {mittlere Geschwindigkeit};
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{v-t-Diagramm für die mittlere Geschwindigkeit.}
\end{figure}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:}
\begin{align*} 
    s_1 &= s_2 = 15\si{\kilo\metre} \\ 
    v_1 &= 60 \si{\kilo\metre/h} \\
    v_2 &= 100 \si{\kilo\metre/h}
\end{align*}

\textit{Gesucht:} Berechne die mittlere Geschwindigkeit. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}

\[
    \text{Mittlere Geschwindigkeit} = \frac{15\si{\kilo\metre} + 15\si{\kilo\metre}}{\frac{15\si{\kilo\metre}}{60\si{\kilo\metre/h}} + \frac{15\si{\kilo\metre}}{100\si{\kilo\metre/h}}}
                                    = \frac{30\si{\kilo\metre}}{0.4\si{h}} = 75\si{\kilo\metre/h} = \underline{\underline{20.83\si{\metre/\second}}}
\]

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Fahrzeug fährt während 15 Minuten 120\si{\kilo\metre/h}, danach 90\si{\kilo\metre} mit 60\si{\kilo\metre/h}. \\\hspace*{\fill}

\textit{Gesucht:} Wie gross ist die mittlere Geschwindigkeit? \\\hspace*{\fill}

\textit{Lösung:}

\[
    \text{Mittlere Geschwindigkeit} = \frac{30\si{\kilo\metre} + 90\si{\kilo\metre}}{\frac{30\si{\kilo\metre}}{120\si{\kilo\metre/h}} + \frac{90\si{\kilo\metre}}{60\si{\kilo\metre/h}}}
                                    = \frac{120\si{\kilo\metre}}{1.75\si{h}} = 68.57\si{\kilo\metre/h} = \underline{\underline{19.05\si{\metre/\second}}}
\]
\newpage

\subsection{Gleichmässig beschleunigte Bewegung ohne Anfangsgeschwindigkeit \texorpdfstring{($v_0 = 0$)}{(v0 = 0)}}

Die Bewegung eines Körpers (Massepunktes) heisst geradlinig gleichmässig beschleunigt, wenn der Körper sich mit einer konstanten Beschleunigung \textit{a} geradlinig bewegt. Wird er konstant beschleunigt, so 
    \textbf{verändert} sich seine \textbf{Geschwindigkeit linear mit der Zeit}. \\\hspace*{\fill}

Bewegt sich ein Körper (Massepunkt) \textbf{geradlinig gleichmässig beschleunigt aus der Ruhe} (d.h. ohne Anfangsgeschwindigkeit), so nimmt seine Geschwindigkeit \textit{v} bei konstanter Beschleunigung \textit{a} 
    proportional zur verstrichenen Zeit zu:
    \begin{equation}
        v = a \cdot t \ [\si{\metre/\second}] \label{eq:1_Geschw. gleichm. beschl. Beweg. ohne. Anfangsgeschw.}
    \end{equation}

Mit $s = \frac{1}{2} \cdot v \cdot t$ und (\ref{eq:1_Geschw. gleichm. beschl. Beweg. ohne. Anfangsgeschw.}) erhalten wir für den in einer beliebigen Zeitspanne \textit{t} zurückgelegten Weg \textit{s} eines 
    gleichmässig beschleunigten Körpers:
    \begin{equation}
        s = \frac{1}{2}a \cdot t^2 \label{eq:Strecke gleichm. beschl. Beweg. ohne. Anfangsgeschw.}
    \end{equation}

Zu guter Letzt kann man die Geschwindigkeit auf einem zweiten Weg berechnen, indem anstatt der Zeit \textit{t} die zurückgelegte Strecke \textit{s} benutzt wird:
    \begin{equation}
        v^2 = 2as \label{eq:2_Geschw. gleichm. beschl. Beweg. ohne. Anfangsgeschw.}
    \end{equation}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            %--------------%
            % Left Picture %
            %--------------%      
    
            % Arc
            \draw[MyLightBlue,very thick] (0,-1) arc (0:45:1) node[anchor=north, shift={(-1mm,-2mm)}] {$45\si{\degree}$};

            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (-1,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (-1,-1) -- (2,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (-1,-1) -- (-1,2) node[anchor=east, shift={(0,-2.5mm)}] {$v \, [\si{\metre/\second}]$}; % y axis
            \draw[thick] (-1,-0.75) -- (-1,-1) -- (-0.75,-1); % make axes look connected
            
            % Line
            \draw[MyLightBlue,very thick] (-1,-1) -- (1.5,1.5);
    
            %----------------%
            % Middle Picture %
            %----------------%      
    
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,-1) -- (6.5,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$s \, [\si{\metre}]$}; % y axis
            \draw[thick] (3.5,-0.75) -- (3.5,-1) -- (3.75,-1); % make axes look connected
            
            % Parabola
            \draw[MyLightBlue,very thick] (3.5,-1) parabola (5.5,1.5) node[anchor=south, shift={(-1mm,0)}] {Parabelast};
    
            %---------------%
            % Right Picture %
            %---------------%      
            
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 9,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (8,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (8,-1) -- (11,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (8,-1) -- (8,2) node[anchor=east, shift={(0,-2.5mm)}] {$a \, [\si{\metre/\second}^2]$}; % y axis
            \draw[thick] (8,-0.75) -- (8,-1) -- (8.25,-1); % make axes look connected
    
            % Line
            \draw[MyLightBlue,very thick] (8,1) -- (10.5,1);
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{v-t-Diagramm für eine gleichmässig beschleunigte Bewegung mit Anfangsgeschwindigkeit gleich null.}
\end{figure}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Radfahrer beschleunigt mit a = 1.2\si{\metre/\second^2} von 0 auf 20\si{\kilo\metre/h}. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:}
\begin{enumerate}
    \item Wie lange braucht er für die Beschleunigung?
    \item Wie weit fährt er dabei?
    \item Wie gross sind Geschwindigkeit und Weg nach der halben Zeit?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item t = $\frac{20\si{\kilo\metre/h}}{3.6 \cdot 1.2\si{\metre/\second^2}} = \underline{\underline{4.63\si{\second}}}$
    \item s = $\left(\frac{1}{2} \cdot 1.2\si{\metre/\second^2}\right) \cdot (4.63\si{\second})^2 = \underline{\underline{12.86\si{\metre}}}$
    \item v = $1.2\si{\metre/\second^2} \cdot \left(\frac{1}{2} \cdot 4.63\si{\second}\right) = \underline{\underline{2.78\si{\metre/\second}}}$ \par
          s = $\left(\frac{1}{2} \cdot 1.2\si{\metre/\second^2}\right) \cdot \left(\frac{1}{2} \cdot 4.63\si{\second}\right) = \underline{\underline{3.22\si{\metre}}}$
\end{enumerate}
\newpage

\subparagraph{Beispiel 2}

\textit{Gegeben:} In dem Augenblick, in dem die Ampel auf grün springt, fährt ein Auto mit konstanter Beschleunigung von 2.2\si{\metre/\second^2} los und behält die Beschleunigung bei.
                  Im selben Moment überholt ein LKW mit konstanten 40\si{\kilo\metre/h} den Wagen. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} In welcher Entfernung wird das Auto den LKW überholen (von der Ampel aus)? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}
\begin{align*}
    s_{LKW} &= v_{LKW} \cdot t = \frac{1}{2}a_{Auto} \cdot t^2 = s_{Auto} \\
    t &= \frac{\frac{40\si{\kilo\metre/h}}{3.6}}{\frac{2.2\si{\metre/\second^2}}{2}} = 10.10\si{\second} \\
    s &= \left(\frac{40\si{\kilo\metre/h}}{3.6}\right) \cdot 10.10\si{\second} = \underline{\underline{112.22\si{\metre}}}
\end{align*}

\subsubsection{Freier Fall}

Auch frei fallende Körper bewegen sich auf der Erde unter bestimmten Bedingungen geradlinig gleichmässig beschleunigt. Hier bestimmt die Erdanziehung (Gravitation) den Wert der Beschleunigung, im Mittel beträgt sie 
    \textit{g} = $-9.81\si{\metre/\second}^2$. Die Erdbeschleunigung \textit{g} wird hier negativ definiert, weil sie nach unten zeigt. \\\hspace*{\fill}

Natürlich gilt dieser Grundsatz nicht nur auf der Erde, sondern auf allen Himmelskörpern. Der Unterschied liegt in der Grösse der Anziehungskraft (also von \textit{g}). \\\hspace*{\fill}

Im Wesentlichen können dieselben Formel wie bei der gleichmässig beschleunigten Bewegung ohne Anfangsgeschwindigkeit verwendet werden, nur dass die Beschleunigung \textit{a} mit der Erdanziehung \textit{g} 
    substituiert wird:
    \begin{gather} 
        v = g \cdot t \label{eq:1_Geschw. Freier Fall} \\ 
        s = \frac{1}{2}g \cdot t^2 \label{eq:Strecke Freier Fall} \\
        v^2 = 2gs \label{eq:2_Geschw. Freier Fall}
    \end{gather}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,0.5) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,0.5) -- (6.5,0.5) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$a \, [\si{\metre/\second}^2]$}; % y axis
            \draw[thick] (3.5,0.75) -- (3.5,0.5) -- (3.75,0.5); % make axes look connected
    
            % Line
            \draw[MyLightBlue,very thick] (3.5,-0.5) -- (6,-0.5) node[pos=0,anchor=east, shift={(0mm,0mm)}] {$-9.81$};
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{v-t-Diagramm für den freien Fall.}
\end{figure}
\newpage

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Von der Spitze eines Turmes lässt man einen Gegenstand fallen, der nach vier Sekunden auf den Boden aufschlägt. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:}
\begin{enumerate}
    \item Wie hoch ist der Turm?
    \item Mit welcher Geschwindigkeit schlägt der Gegenstand auf den Boden auf?
    \item Nach welcher Zeit hat der Gegenstand die Hälfte seines Fallweges zurücklegt?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item s = $\frac{9.81\si{\metre/\second^2}}{2} \cdot (4\si{\second})^2 = \underline{\underline{78.48\si{\metre}}}$
    \item v = $9.81\si{\metre/\second^2} \cdot 4\si{\second} = \underline{\underline{39.24\si{\metre/\second}}}$
    \item t = $\sqrt{\frac{\frac{78.48\si{\metre}}{2}}{\frac{9.81\si{\metre/\second^2}}{2}}} = \underline{\underline{2.83\si{\second}}}$
\end{enumerate}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Eine Katze fällt einen sieben Meter hohen Baum herunter und landet auf den Füssen. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:}
\begin{enumerate}
    \item Wie lange hat sie, bis sie unten ankommt?
    \item Wie schnell ist sie, wenn sie unten ankommt?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item t = $\sqrt{\frac{7\si{\metre}}{\frac{9.81\si{\metre/\second^2}}{2}}} = \underline{\underline{1.19\si{\second}}}$
    \item v = $9.81\si{\metre/\second^2} \cdot 1.19\si{\second} = \underline{\underline{11.72\si{\metre/\second^2}}}$
\end{enumerate}

\subparagraph{Beispiel 3}

\textit{Gegeben:} James Bond lässt sich von einer Brücke auf das 5.5 Meter unter ihm liegende Dach eines fahrenden Zuges fallen. Der Zug ist mit 108\si{\kilo\metre/h} unterwegs. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Welche Distanz muss er zum Bösewicht haben, um exakt auf ihn zu landen? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}

\[ s = \sqrt{\frac{5.5\si{\metre}}{\frac{9.81\si{\metre/\second^2}}{2}}} \cdot \frac{108\si{\kilo\metre/h}}{3.6} = \underline{\underline{31.77\si{\metre}}}\]
\newpage

\subsection{Gleichmässig beschleunigte Bewegung mit Anfangsgeschwindigkeit \texorpdfstring{($v_0 \neq 0$)}{(v0 ungleich 0)}}

Hat der geradlinig gleichmässig beschleunigt bewegte Körper zu Beginn der Untersuchung seiner Bewegung schon eine Anfangsgeschwindigkeit, so ändern sich die Formeln nur geringfügig: Bei den Formeln aus der 
    gleichmässig beschleunigten Bewegung ohne Anfangsgeschwindigkeit muss die Anfangsgeschwindigkeit \textit{$v_0$} hinzugefügt werden. Somit lauten die modifizierten Formeln wie folgt:
    \begin{gather} 
        v = v_0 + a \cdot t \label{eq:1_Geschw. gleichm. beschl. Beweg. mit Anfangsgeschw.} \\ 
        s = v_0 \cdot t + \frac{1}{2}a \cdot t^2 \label{eq:Strecke gleichm. beschl. Beweg. mit Anfangsgeschw.} \\
        v^2 = v_0^2 + 2as \label{eq:2_Geschw. gleichm. beschl. Beweg. mit Anfangsgeschw.}
    \end{gather}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            %--------------%
            % Left Picture %
            %--------------%      
    
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (-1,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (-1,-1) -- (2,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (-1,-1) -- (-1,2) node[anchor=east, shift={(0,-2.5mm)}] {$v \, [\si{\metre/\second}]$}; % y axis
            \draw[thick] (-1,-0.75) -- (-1,-1) -- (-0.75,-1); % make axes look connected
            
            % Line
            \draw[MyLightBlue,very thick] (-1,-0.5) -- (1.5,1.5) node[anchor=east, shift={(-25mm,-20mm)}] {$10$};
    
            %----------------%
            % Middle Picture %
            %----------------%      
    
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,-1) -- (6.5,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$s \, [\si{\metre}]$}; % y axis
            \draw[thick] (3.5,-0.75) -- (3.5,-1) -- (3.75,-1); % make axes look connected
            
            % Parabola
            \draw[MyLightBlue,very thick] (3.5,-1) parabola (4.5,1.5) node[anchor=south, shift={(1mm,0)}] {Parabelast};
    
            %---------------%
            % Right Picture %
            %---------------%      
            
            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 9,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (8,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (8,-1) -- (11,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (8,-1) -- (8,2) node[anchor=east, shift={(0,-2.5mm)}] {$a \, [\si{\metre/\second}^2]$}; % y axis
            \draw[thick] (8,-0.75) -- (8,-1) -- (8.25,-1); % make axes look connected
    
            % Line
            \draw[MyLightBlue,very thick] (8,1) -- (10.5,1);
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{v-t-Diagramm für eine gleichmässig beschleunigte Bewegung mit Anfangsgeschwindigkeit ungleich null.}
\end{figure}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein fallender Stein benötigt 0.3 Sekunden, um ein 1.8 Meter hohes Fenster zu passieren. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} In welcher Höhe oberhalb der oberen Fensterkante wurde der Stein fallen gelassen? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}
\begin{align*} 
    v_0 &= \frac{s - \frac{1}{2}a \cdot t^2}{t} = \frac{1.8\si{\metre} - \frac{9.81\si{\metre/\second}^2}{2} \cdot (0.3\si{\second})^2}{0.3\si{\second}} = 4.53\si{\metre/\second} \\ 
      s &= \frac{v^2 - v_0^2}{2a} = \frac{(0\si{\metre/\second})^2 - (4.53\si{\metre/\second})^2}{2 \cdot -9.81\si{\metre/\second^2}} = \underline{\underline{1.05\si{\metre}}}
\end{align*}
\newpage

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Auto fährt mit v = 30\si{\kilo\metre/h} als plötzlich ein Hindernis auftaucht. Nach einer Reaktionszeit von 1 Sekunde bremst das Auto mit
                  a = $-3.0\si{\metre/\second}^2$ ab und bleibt vor dem Hindernis stehen. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)
                  
\textit{Gesucht:}
\begin{enumerate}
    \item Zeichne das v-t-Diagramm.
    \item Welche Strecke fährt das Auto während der Reaktionszeit (also der Reaktionsweg)?
    \item Wie gross ist der Bremsweg?
    \item Wie gross ist der Anhalteweg?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}
    \item \leavevmode\vadjust{\vspace{-\baselineskip}}
              \begin{flushleft}
                \begin{tikzpicture}[>=latex] % arrow style
            
                    % Axis
                    \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,-1) {$+$};} % axis ticks x axis
                    \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
                    \draw[thick,->] (3.5,-1) -- (6.5,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
                    \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$v \, [\si{\metre/\second}]$}; % y axis
                    \draw[thick] (3.5,-0.75) -- (3.5,-1) -- (3.75,-1); % make axes look connected

                    % Line
                    \draw[MyLightBlue,very thick] (3.5,1) -- (4,1) -- (6,-1);
                    \draw[MyLightBlue,very thick] (4,1.125) -- (4,0.875) node[anchor=south, shift={(0,2.5mm)}] {$1 \si{\second}$};
                    \draw[MyLightBlue,very thick] (6,-0.875) -- (6,-1.125) node[anchor=south, shift={(0,2.5mm)}] {$11 \si{\second}$};
            
                \end{tikzpicture}
            \end{flushleft}
    \item \underline{\underline{30\si{\metre}}} (da das Auto 30 Meter pro Sekunde fährt)
    \item \begin{align*}
              t &= \frac{v - v_0}{a} = \frac{0\si{\metre/\second} - 30\si{\metre/\second}}{-3\si{\metre/\second}^2} = 10\si{\second} \\
              s &= 30\si{\metre/\second} \cdot 10\si{\second} + \left(\frac{1}{2} \cdot -3\si{\metre/\second}^2\right) \cdot (10\si{\second})^2 = \underline{\underline{150\si{\metre}}}
          \end{align*}
    \item s = 30\si{\metre} + 150\si{\metre} = \underline{\underline{150\si{\metre}}}
\end{enumerate}
\newpage

\subsubsection{Vertikaler Wurf/Senkrechter Wurf}

Unter einem senkrechten Wurf wird die Bewegung eines Körpers, der senkrecht in die Höhe geworfen wird, verstanden. Dabei kommt es nicht nur auf die Anfangsgeschwindigkeit, sondern auch auf die lokale 
    Fallbeschleunigung, den Ortsfaktor, an. Sobald der Körper die Hand verlässt, nimmt seine Geschwindigkeit ständig ab, bis sie am Umkehrpunkt sogar kurzfristig Null ist. Ab jetzt bewegt sich der Körper mit zunehmender negativer Geschwindigkeit nach unten. Die Geschwindigkeit eines vertikalen Wurfes ist an seinem Start- und Endpunkt dieselbe. \\\hspace*{\fill}

Auch hier müssen die Formeln der gleichmässig beschleunigten Bewegung mit Anfangsgeschwindigkeit nur geringfügig angepasst werden:
    \begin{gather} 
        v = v_0 + g \cdot t \label{eq:1_Geschw. senkrechter Wurf} \\ 
        s = v_0 \cdot t + \frac{1}{2}g \cdot t^2 \label{eq:Strecke senkrechter Wurf} \\
        v^2 = v_0^2 + 2gs \label{eq:2_Geschw. senkrechter Wurf}
    \end{gather}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            %--------------%
            % Left Picture %
            %--------------%      

            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i,0.5) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (-1,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (-1,0.5) -- (2,0.5) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (-1,-1) -- (-1,2) node[anchor=east, shift={(0,-2.5mm)}] {$v \, [\si{\metre/\second}]$}; % y axis
            \draw[thick] (-1,0.75) -- (-1,0.5) -- (-0.75,0.5); % make axes look connected

            % Line
            \draw[MyLightBlue,very thick] (-1,-0.5) -- (1.5,-0.5) node[pos=0,anchor=east, shift={(0mm,0)}] {$-9.81$};

            %----------------%
            % Middle Picture %
            %----------------%      

            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 4.5,0.5) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,0.5) -- (6.5,0.5) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$s \, [\si{\metre}]$}; % y axis
            \draw[thick] (3.5,0.75) -- (3.5,0.5) -- (3.75,0.5); % make axes look connected

            % Line
            \draw[MyLightBlue,very thick,loosely dashed] (3.5,-0.5) -- (6,-0.5) node[pos=0,anchor=east, shift={(0mm,0)}] {$-20$};
            \draw[MyLightBlue,very thick] (3.5,1.5) -- (6,-0.5) node[pos=0,anchor=east, shift={(0mm,-1.5mm)}] {$20$};

            %---------------%
            % Right Picture %
            %---------------%      

            % Axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (\i + 9,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (8,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (8,-1) -- (11,-1) node[anchor=north, shift={(0,0)}] {$t \, [\si{\second}]$}; % x axis
            \draw[thick,->] (8,-1) -- (8,2) node[anchor=east, shift={(0,-2.5mm)}] {$a \, [\si{\metre/\second}^2]$}; % y axis
            \draw[thick] (8,-0.75) -- (8,-1) -- (8.25,-1); % make axes look connected

            % Line/Arc
            \draw[MyLightBlue,very thick,loosely dashed] (8,1) -- (9.25,1) node[anchor=south, shift={(0,0)}, text width=2cm,align=center] {maximaler\\Steigpunkt};
            \draw[MyLightBlue,very thick,loosely dashed] (9.25,1) -- (9.25,-1) node[anchor=south, shift={(0,-7mm)}, text=black] {$\frac{t}{2}$};
            \draw[MyLightBlue,very thick] (8,-1) parabola bend (9.25,1) (10.5,-1);
    
        \end{tikzpicture}
    \end{center}

    \vspace{-1em}
    \caption{v-t-Diagramm für den vertikalen Wurf.}
\end{figure}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Stein wird mit $v_0$ = 18\si{\metre/\second} nach oben geworfen. \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Gesucht:} Welche Geschwindigkeit hat er beim Zurückfallen vier Meter oberhalb der Abwurfstelle? \\\hspace*{\fill} % Fixes error underfull \hbox (badness 10'000)

\textit{Lösung:}

\[
    v = \sqrt{(18\si{\metre/\second})^2 + 2 \cdot 9.81\si{\metre/\second}^2 \cdot 4\si{\metre}} = \underline{\underline{15.67\si{\metre/\second}}}
\]
\newpage